# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.25] - 2018-09-16
### Changed
- README notes project movement

## [1.4.24] - 2018-07-17
### Changed
- Removing uglified code due to the uglifyjs package breaking command parsing

## [1.4.23] - 2018-07-17
### Changed
- Fixing missing files because of NPM desync

## [1.4.22] - 2018-07-17
### Changed
- Fixed uglify process

## [1.4.21] - 2018-07-17
### Changed
- Resync with NPM

## [1.4.20] - 2018-07-17
### Changed
- Fix version short from being removed from help

## [1.4.19] - 2018-07-16
### Changed
- Add back in dist files

## [1.4.18] - 2018-07-08

### Added
- Short option description for `option` function in README

## [1.4.17] - 2018-03-11
### Changed
- Events can also be async functions or promises

## [1.4.16] - 2018-03-11
### Changed
- Fix skipping parameters for when the number of arguments is less than the number of possible parameters

## [1.4.15] - 2018-03-11
### Changed
- Fix for parsing twice if process is exited early

## [1.4.14] - 2018-03-11
### Changed
- Wait for promises to end before continuing interactive prompt
- Fix for non-interactive binaries exiting before a promise command completes

## [1.4.13] - 2018-03-09
### Changed
- Fix for ArgParser sub commands onResult callbacks being called twice

### Added
- Async function command and Promise command support

## [1.3.13] - 2018-03-07
### Changed
- Fix for `helpOptionDescription` sometimes not setting the description
- `parse` can be called without an ArgParser from being created beforehand

### Added
- Build CLI via a config object with the `.from` function
- Example CLI config object in examples/exampleConfig.js

## [1.2.13] - 2018-03-05
### Changed
- Fixed help output for command parameter spacing

## [1.2.12] - 2018-03-05
### Changed
- Fixed removing switch character in option values

### Added
- Add option parameter via the option name

## [1.2.11] - 2018-03-04
### Changed
- Fix for default commands not recognizing parameters

## [1.2.10] - 2018-03-03
### Changed
- Fixed parsing twice for non-interactive CLIs when calling parse manually
- Result callback always executes on parse
- Add sub command parameters to results
- README updates

## [1.2.9] - 2018-03-02
### Changed
- Better support for Node 4
- Minified for a smaller footprint

## [1.2.8] - 2018-03-01
### Changed
- Fixed some options being ignored

## [1.2.7] - 2018-03-01
### Changed
- Allow all commands to parse their options and parameters

### Added
- Pass through options

## [1.1.7] - 2018-02-20
### Changed
- Fixed issue with function commands not recognizing missing arguments

## [1.1.6] - 2018-02-20
### Changed
- Fixed issue with function commands not being updated with the arguments

## [1.1.5] - 2018-02-20
### Changed
- README updates

## [1.1.4] - 2018-02-12
### Changed
- Fixed custom help and version options not being recognized

## [1.1.3] - 2018-02-12
### Changed
- Fixed quit command not firing on first call

## [1.1.2] - 2018-02-12
### Changed
- Fixed missing required errors blocking quit command

## [1.1.1] - 2018-02-12
### Changed
- Fixed help option clashing with missing arguments output

## [1.1.0] - 2018-02-12

### Added
- Require config for options
- Missing required parameters message

## [1.0.0] - 2018-01-26

### Added
- 1.0.0 release

## [0.10.40] - 2018-01-25
### Changed
- Handle tab characters in indentation

## [0.10.39] - 2018-01-25

### Added
- Help output sections are indented according to initial indent

## [0.9.39] - 2018-01-19

### Added
- Node 4+ support
- Version option name and short override function
- Help option name and short override function

## [0.8.39] - 2018-01-19
### Changed
- Fix issue introduced in last patch with parameter help output

## [0.8.38] - 2018-01-19
### Changed
- Small code cleanup
- More number value fixes

## [0.8.37] - 2018-01-19
### Changed
- Parameter separator parsing in function commands fix
- Handle number values better in options/paramters

## [0.8.36] - 2018-01-17
### Changed
- Fix setRawMode not being a function

## [0.8.35] - 2018-01-17
### Changed
- Version text effects

## [0.8.34] - 2018-01-17

### Added
- Interactive CLI input history

## [0.7.34] - 2018-01-16
### Changed
- Mention using pkg in README as a tool to generate a binary

## [0.7.33] - 2018-01-12
### Changed
- Fix for command files in subdirectories

## [0.7.32] - 2018-01-12
### Changed
- Fixed help output printing at wrong time in interactive CLIs

## [0.7.31] - 2018-01-12
### Changed
- Fixed issue with binary .exe commands

## [0.7.30] - 2018-01-12
### Changed
- Additional fix for binary sub commands command function parameters

## [0.7.29] - 2018-01-12
### Changed
- Better handling of finding files
- Fixed passing parameters to binary sub commands via the command function

## [0.7.28] - 2018-01-12
### Changed
- Fix binary command file lookup

## [0.7.27] - 2018-01-12
### Changed
- Missing files added

## [0.7.26] - 2018-01-12

### Added
- Binary command file support

## [0.6.26] - 2018-01-11
### Changed
- Support for exe binaries and extra Windows support

## [0.6.25] - 2018-01-11
### Changed
- Support for execution from binaries

## [0.6.24] - 2018-01-11
### Changed
- Format Object and Array option defaults help output

## [0.6.23] - 2018-01-11
### Changed
- Improved handling of text effects

### Added
- Additional text effects

## [0.5.23] - 2018-01-11
### Changed
- Allow negative switch option to set positive option even if the positive option does not exist

## [0.5.22] - 2018-01-11
### Changed
- Allow empty arrays for args

## [0.5.21] - 2018-01-11
### Changed
- Fixed parsing of number values for parameters and options
- Fixed paramater parsing when a switch character is set

## [0.5.20] - 2018-01-11
### Changed
- Caller file fixed
- Multiple version outputs bug fixed
- Non-existent option short crash bug fix

## [0.5.19] - 2018-01-10

### Added
- Unit tests

## [0.4.19] - 2018-01-10

### Added
- Contributing
- Test framework

## [0.4.18] - 2018-01-10

### Added
- README badges

## [0.4.17] - 2018-01-10
### Changed
- Fixing parsing options and parameters

## [0.4.16] - 2018-01-10
### Changed
- Commands ignore subcommand options

### Added
- Custom help description
- Custom option description
- Extra options passed to callbacks
- Extra parameters passed to callbacks

## [0.3.16] - 2018-01-09
### Changed
- Fixed usage parameter separator

## [0.3.15] - 2018-01-09
### Changed
- More temporary fixes

## [0.3.14] - 2018-01-09
### Changed
- Quick temporary fix for parsing commands

## [0.3.13] - 2018-01-09
### Changed
- Minor README update

## [0.3.12] - 2018-01-09
### Changed
- Fixed parameter parsing in command function

## [0.3.11] - 2018-01-09
### Changed
- Fix callback functions having undefined parameters

## [0.3.10] - 2018-01-09
### Changed
- Fixed parameter parsing for both interactive and non interactive CLIs

## [0.3.9] - 2018-01-08
### Changed
- Fixes for parsing and executing function actions with parameters

## [0.3.8] - 2018-01-08
### Changed
- Parameter parsing fixes and various bug fixes

## [0.3.7] - 2018-01-08
### Changed
- Fix default command being executed even if help or version options are provided

## [0.3.6] - 2018-01-08
### Changed
- Fixed missing parameter separators in help usage line

## [0.3.5] - 2018-01-08
### Changed
- Default command bug fixes

## [0.3.4] - 2018-01-08
### Changed
- Command optional parameter parsing fix

## [0.3.3] - 2018-01-08
### Changed
- Command parameter bug fix

## [0.3.2] - 2018-01-08
### Changed
- Command parameter separators

## [0.2.2] - 2018-01-08
### Changed
- Code restructure
- Command and option descriptions can be modified with text effects

## [0.1.2] - 2018-01-05
### Changed
- Easier to spot separation of API functions

## [0.1.1] - 2018-01-05

### Added
- Changelog

## 0.1.0 - 2018-01-05

### Added
- Beta version release

[Unreleased]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.25...HEAD
[1.4.25]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.24...v1.4.25
[1.4.24]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.23...v1.4.24
[1.4.23]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.22...v1.4.23
[1.4.22]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.21...v1.4.22
[1.4.21]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.20...v1.4.21
[1.4.20]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.20...v1.4.20
[1.4.19]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.18...v1.4.19
[1.4.18]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.17...v1.4.18
[1.4.17]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.16...v1.4.17
[1.4.16]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.15...v1.4.16
[1.4.15]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.14...v1.4.15
[1.4.14]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.4.13...v1.4.14
[1.4.13]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.3.13...v1.4.13
[1.3.13]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.2.13...v1.3.13
[1.2.13]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.2.12...v1.2.13
[1.2.12]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.2.11...v1.2.12
[1.2.11]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.2.10...v1.2.11
[1.2.10]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.2.9...v1.2.10
[1.2.9]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.2.8...v1.2.9
[1.2.8]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.2.7...v1.2.8
[1.2.7]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.1.7...v1.2.7
[1.1.7]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.1.6...v1.1.7
[1.1.6]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.1.5...v1.1.6
[1.1.5]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.1.4...v1.1.5
[1.1.4]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.1.3...v1.1.4
[1.1.3]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.1.2...v1.1.3
[1.1.2]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.1.1...v1.1.2
[1.1.1]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/jsonsonson/wily-cli/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.10.40...v1.0.0
[0.10.40]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.10.39...v0.10.40
[0.10.39]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.9.39...v0.10.39
[0.9.39]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.8.39...v0.9.39
[0.8.39]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.8.38...v0.8.39
[0.8.38]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.8.37...v0.8.38
[0.8.37]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.8.36...v0.8.37
[0.8.36]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.8.35...v0.8.36
[0.8.35]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.8.34...v0.8.35
[0.8.34]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.7.34...v0.8.34
[0.7.34]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.7.33...v0.7.34
[0.7.33]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.7.32...v0.7.33
[0.7.32]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.7.31...v0.7.32
[0.7.31]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.7.30...v0.7.31
[0.7.30]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.7.29...v0.7.30
[0.7.29]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.7.28...v0.7.29
[0.7.28]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.7.27...v0.7.28
[0.7.27]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.7.26...v0.7.27
[0.7.26]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.6.26...v0.7.26
[0.6.26]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.6.25...v0.6.26
[0.6.25]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.6.24...v0.6.25
[0.6.24]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.6.23...v0.6.24
[0.6.23]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.5.23...v0.6.23
[0.5.23]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.5.22...v0.5.23
[0.5.22]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.5.21...v0.5.22
[0.5.21]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.5.20...v0.5.21
[0.5.20]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.5.19...v0.5.20
[0.5.19]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.4.19...v0.5.19
[0.4.19]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.4.18...v0.4.19
[0.4.18]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.4.17...v0.4.18
[0.4.17]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.4.16...v0.4.17
[0.4.16]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.16...v0.4.16
[0.3.16]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.15...v0.3.16
[0.3.15]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.14...v0.3.15
[0.3.14]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.13...v0.3.14
[0.3.13]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.12...v0.3.13
[0.3.12]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.11...v0.3.12
[0.3.11]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.10...v0.3.11
[0.3.10]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.9...v0.3.10
[0.3.9]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.8...v0.3.9
[0.3.8]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.7...v0.3.8
[0.3.7]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.6...v0.3.7
[0.3.6]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.5...v0.3.6
[0.3.5]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.4...v0.3.5
[0.3.4]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.3...v0.3.4
[0.3.3]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.3.2...v0.3.3
[0.3.2]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.2.2...v0.3.2
[0.2.2]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.1.2...v0.2.2
[0.1.2]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/jsonsonson/wily-cli/compare/v0.1.0...v0.1.1
