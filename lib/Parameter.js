const values = require('./values.js');

class Parameter {

  constructor(name, required, repeated, argParser) {
    this._n = name;
    this.required = required;
    this.repeated = repeated;
    this.argParser = argParser;
    this.built = false;
  }

  build() {
    if (!this.built) {
      this.built = true;

      this.name = this._n.replace(values.invalidCLIChars, '').trim();

      if (this.argParser !== undefined) {
        this.argParser.cli.parameters[this.name] = this;
        delete this.argParser;
      }
    }
  }

  toString() {
    if (this.required) {
      return `<${this.name}${this.repeated ? '...' : ''}>`;
    } else {
      return `[${this.name}${this.repeated ? '...' : ''}]`;
    }
  }

  help() {
    return this.toString();
  }
}

module.exports = Parameter;
