const values = require('./values.js');
const buildParameters = require('./builders.js').buildParameters;
const Parameter = require('./Parameter.js');
const path = require('path');
const fs = require('fs');
const { formatStringToConsole, executeResultCallback } = require('./helpers');
const globalOptions = require('./globalOptions.js');

function getParameters(command, commandObj) {
  let cmd = `${command}`;
  const optionalParamInd = cmd.indexOf('[') >= 0 ? cmd.indexOf('[') : cmd.length;
  const requiredParamInd = cmd.indexOf('<') >= 0 ? cmd.indexOf('<') : cmd.length;
  const paramInd = optionalParamInd < requiredParamInd ? optionalParamInd : requiredParamInd;
  let parameters;

  if (paramInd !== cmd.length) {
    const _cmdParams = cmd.substr(paramInd, cmd.length).trim();
    let cmdParams = [];

    const matches = [];
    const regex = /[<|[]+[^>\]]+[>|\]]+?/g;
    let match;

    while (match = regex.exec(_cmdParams)) {
      matches.push(match[0]);
    }

    if (matches.length > 1) {
      const firstMatchEndInd = matches[0].length;
      const secondMatchStartInd = _cmdParams.indexOf(matches[1]);
      let separator = _cmdParams.substring(firstMatchEndInd, secondMatchStartInd);

      if (separator !== undefined && separator.length > 0) {
        separator = separator.trim();
        commandObj.paramSeparator = `${separator}`;
        commandObj.argParser.paramSeparator = `${separator}`;
        commandObj._action.paramSeparator = `${separator}`;
      }
    }

    if (matches.length > 0) {
      cmdParams.push(...matches);
    }

    cmdParams.forEach(param => {
      if (parameters === undefined) {
        parameters = {};
      }

      const required = param.charAt(0) === '<';
      const repeated = param.substr(param.length - 4, 3).trim() === '...';
      const paramName = param.substr(1, param.length - 2).replace('...', '');
      parameters[paramName] = new Parameter(paramName, required, repeated);
    });

    cmd = cmd.substr(0, paramInd).trim();
  }

  return {
    parameters,
    cmd,
  };
}

function _buildParameters(paramObj) {
  const paramKeys = Object.keys(paramObj);
  paramKeys.forEach(param => paramObj[param].build());
}

function findFile(dir, fileName) {
  let file = path.join(dir, fileName);

  if (!['exe', 'js'].includes(path.extname(file).toLowerCase())) {
    file = fileWithFileType(file);
  }

  return file;
}

function fileWithFileType(fileName) {
  let file = `${fileName}`;

  if (fs.existsSync(`${fileName}.js`)) {
    file = `${fileName}.js`;
  } else if (fs.existsSync(`${fileName}.exe`)) {
    file = `${fileName}.exe`;
  }

  return file;
}

class Command {

  constructor(name, description, action, argParser) {
    this.projectDir = path.parse(process.mainModule.filename || process.mainModule.id).dir;
    this._n = name;
    this.description = description;
    this._action = action;
    this.argParser = argParser;
    this.built = false;
    this.callerFile = this.argParser.callerFile;
    this.paramSeparator = ' ';

    if (this._action !== undefined && this._action.constructor.name === 'ArgParser') {
      action.isFirstCommand = false;
      this._action.isFirstCommand = false;
    }
  }

  build() {
    if (!this.built) {
      this.built = true;
      this.name = this._n.replace(values.invalidCLIChars, '').trim();

      if (!this.argParser.cli.commands[this.name]) {
        if (this._action === undefined) {
          const p = getParameters(this.name, this);

          if (p.parameters !== undefined) {
            _buildParameters(p.parameters);
          }

          this.name = p ? p.cmd || this.name : this.name;
          this.params = p.parameters;

          let fileName = path.basename(this.argParser.callerFile, path.extname(this.argParser.callerFile));
          fileName = `${fileName}-${this.name}`;
          this._action = fileName;
        }

        if (this._action.constructor === String) {
          const dir = path.dirname(this.callerFile);
          const file = findFile(dir, `${this._action}`);

          if (file.split('.').pop().toLowerCase() === 'js') {
            this._action = require(file);
            this._action.callerFile = file;
          } else {
            this.callerFile = file;
            this._action = file;
          }
        }

        if (this._action.constructor.name === 'AsyncFunction' || this._action.constructor.name === 'Promise') {
          const asyncAction = this._action;
          const isFirstCommand = this._action.isFirstCommand;
          const commandInd = this._action.commandInd;

          this._action = (options, parameters, command) => {
            return asyncAction.constructor.name === 'Promise' ?
              asyncAction : new Promise((resolve, reject) => {
                asyncAction(options, parameters, command)
                  .then(resolved => {
                    resolve(resolved);
                  })
                  .catch(err => {
                    reject(err);
                  });
              });
          };

          this._action.isFirstCommand = isFirstCommand;
          this._action.commandInd = commandInd;
          this._action.isPromise = true;
        }

        if (this._action.constructor === Function) {
          const p = getParameters(this.name, this);

          if (!this.params && p.parameters !== undefined) {
            _buildParameters(p.parameters);
          }

          this.name = p ? p.cmd || this.name : this.name;

          const actionFunc = this._action;
          this._action = (options, parameters, command) => {
            const onResultOld = this.argParser.resultCallback;
            this.argParser.resultCallback = results => {
              const argParams = [];

              Object.keys(results.parameters).forEach(p => {
                argParams.push(results.parameters[p]);
              });

              const subCommands = this.argParser.commands.map(command => command.name);
              const newInd = this.argParser.argv.findIndex(arg => `${arg}`.trim() !== this.name && subCommands.includes(`${arg}`.trim()));
              const callerFileInd = this.argParser.argv.findIndex(arg => `${arg}`.trim() === this.argParser.callerFile);
              this.argParser.resultCallback = onResultOld;

              if (this.argParser.interactive && newInd >= 0) {
                const newArgs = this.argParser.argv.filter((arg, index) => index <= callerFileInd || index >= newInd);

                if (newArgs.length !== this.argParser.argv.length) {
                  this.argParser.parse(newArgs);
                }
              }

              if (this.argParser.resultCallback !== undefined && this.argParser.resultCallback.constructor === Function) {
                executeResultCallback(this.argParser, results);
              }
            };

            if (actionFunc.isPromise) {
              this.argParser.waitingForCommandPromise = true;
              actionFunc(options, parameters, command)
                .then(() => {
                  this.argParser.commandPromiseComplete = true;
                })
                .catch(err => {
                  this.argParser.commandPromiseComplete = true;

                  if (this.argParser.onError !== undefined && this.argParser.onError.constructor === Function) {
                    this.argParser.onError(err);
                  }
                });
            } else {
              try {
                actionFunc(options, parameters, command);
              } catch (err) {
                if (this.argParser.onError !== undefined && this.argParser.onError.constructor === Function) {
                  this.argParser.onError(err);
                }
              }
            }
          };

          this._action.commandName = this.name;
          this._action.commandList = [];

          if (!this.params && p.parameters) {
            this.params = p.parameters;
          }

          if (this.params !== undefined) {
            this._action.cli = {
              parameters: this.params,
            };
          }

          if (this.paramSeparator !== undefined) {
            this._action.paramSeparator = this.paramSeparator;
          }

          this.action = this._action;
          this.action.missingRequiredEnabled = this.argParser.missingRequiredEnabled;
          this.action.missingRequiredMessage = this.argParser.missingRequiredMessage;
          this.action.optionsEnabled = this.argParser.optionsEnabled;
          this.action.commandsEnabled = this.argParser.commandsEnabled;
          this.action.helpOpt = this.argParser.helpOpt;
          this.action.isFirstCommand = false;
          this.argParser.subCommands.push(this.action);
        } else if (this._action.constructor.name === 'ArgParser') {
          if (this._action.parameters !== undefined && this._action.parameters.length > 0) {
            buildParameters(this._action);
            this.params = this._action.cli.parameters;
          } else {
            const p = this.name.split(' ').length > 1 ? getParameters(this.name, this) : {};

            if (!this.params) {
              if (p.parameters !== undefined) {
                _buildParameters(p.parameters);
              }

              this.params = p.parameters || undefined;
            }

            this.name = p ? p.cmd || this.name : this.name;

            if (this.params !== undefined) {
              this._action.cli.parameters = this.params;
            }
          }

          this._action.commandName = this.name;

          if (!this._action.commandInd) {
            this._action.commandInd = this.argParser.commandInd + 1;
          }

          this.argParser.subCommands.push(this._action);
          this._action.interactive = this.argParser.interactive || false;
          this.action = () => {
            this._action.result = this._action.parse(this.argParser.argv);
          };
          this.action.isFirstCommand = false;
          this.action.commandName = this.name;
          this.action.commandInd = this._action.commandInd;
          this.action.callerFile = this._action.callerFile;
        } else {
          // Is binary
          const p = this.name.split(' ').length > 1 ? getParameters(this.name, this) : {};

          if (!this.params) {
            if (p.parameters !== undefined) {
              _buildParameters(p.parameters);
            }

            this.params = p.parameters || undefined;
          }

          this.name = p ? p.cmd || this.name : this.name;

          this.argParser.subCommands.push(this._action);
          this.action = () => {
            const { execSync } = require('child_process');

            const args = this.argParser.argv.slice(this.argParser.argv.indexOf(this.name) + 1);
            const file = this._action;

            const initialCommand = require('./helpers.js').initialCommand(this.argParser) || (this.argParser.interactive ? ' ' : '');
            let previousCommands = process.env.previousCommands || this.name;

            if (previousCommands !== this.name) {
              previousCommands = `${previousCommands}{[~~]}${this.name}`;
            }

            execSync(`${file} ${args.join(' ')}`, {
              env: {
                previousCommands,
                argv0: process.argv0,
                initialCommand: process.env.initialCommand || initialCommand,
                params: this.params ? JSON.stringify(this.params) : '',
                globalOptions: globalOptions.get() ? JSON.stringify(globalOptions.get()) : '',
              },
              stdio: 'inherit',
            });
          };

          this.action.isFirstCommand = false;
          this.argParser.isFirstCommand = false;
          this.action.commandName = this.name;
          this.action.commandInd = this.argParser.commandInd + 1;
          this.action.callerFile = this._action.callerFile;
        }

        this.argParser.cli.commands[this.name] = this;
      }
    }
  }

  buildParams() {
    if (!this.built && !this.name) {
      this.name = this._n.replace(values.invalidCLIChars, '').trim();

      if (!this.argParser.cli.commands[this.name]) {
        if (this._action === undefined) {
          const p = getParameters(this.name, this);

          if (p.parameters !== undefined) {
            _buildParameters(p.parameters);
          }

          this.name = p ? p.cmd || this.name : this.name;
          this.params = p.parameters;

          let fileName = path.basename(this.argParser.callerFile, path.extname(this.argParser.callerFile));
          fileName = `${fileName}-${this.name}`;
          this._action = fileName;
        }

        if (this._action.constructor === String) {
          const dir = path.dirname(this.callerFile);
          const file = findFile(dir, `${this._action}`);

          if (file.split('.').pop().toLowerCase() === 'js') {
            this._action = require(file);
          }
        }

        if (this._action.parameters !== undefined && this._action.parameters.length > 0) {
          buildParameters(this._action);
          this.params = this._action.cli.parameters;
        } else {
          const p = this.name.split(' ').length > 1 ? getParameters(this.name, this) : {};

          if (!this.params) {
            if (p.parameters !== undefined) {
              _buildParameters(p.parameters);
            }

            this.params = p.parameters || undefined;
          }

          this.name = p ? p.cmd || this.name : this.name;
        }

        if (this._action.constructor !== String) {
          this._action.isFirstCommand = false;
        }
      }
    }
  }

  toString() {
    this.buildParams();

    if (this.name === undefined) {
      this.name = this._n.replace(values.invalidCLIChars, '').trim();
    }

    let cmdStr = `${this.name}`;

    if (this.params !== undefined) {
      const params = [];

      Object.keys(this.params).forEach(param => {
        this.params[param].build();
        params.push(this.params[param].help());
      });

      let paramSeparator = this.paramSeparator || ' ';

      if (this._action !== undefined && this._action.constructor !== String && this._action.paramSeparator !== undefined && this._action.paramSeparator.trim() !== '') {
        paramSeparator = this._action.paramSeparator;
      }

      cmdStr = `${cmdStr} ${params.join(paramSeparator)}`;
    }

    this.cmdStr = cmdStr;

    return cmdStr;
  }

  help(longestCommand) {
    const lengthDiff = longestCommand - this.cmdStr.length;
    const padding = Array(lengthDiff).join(' ');
    const before = `${this.argParser.helpPaddingStr}  ${values.colors.yellow}${this.cmdStr}${padding}  `;
    const after = formatStringToConsole(`${values.colors.white}${this.description}${values.colors.clear}`, before);

    return `${before}${after}`;
  }
}

module.exports = Command;
