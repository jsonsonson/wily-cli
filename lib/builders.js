function buildOptions(argParser) {
  if (argParser.options !== undefined) {
    argParser.options.forEach((option) => {
      option.build();

      argParser.cli.options[option.name] = option;

      if (option.short !== undefined) {
        argParser.cli.options._shorts[option.short] = option.name;
      }
    });
  }
}

function buildCommand(argParser, command) {
  if (argParser.commands !== undefined) {
    const cmdInd = argParser.commands.findIndex(cmdObj => cmdObj._n.indexOf(command) === 0);
    const cmd = argParser.commands[cmdInd];

    if (cmd !== undefined) {
      cmd.paramSeparator = argParser.paramSeparator;
      cmd.build();
      argParser.cli.commands[cmd.name] = cmd;
    }
  }
}

function buildParameters(argParser) {
  if (argParser.parameters !== undefined) {
    argParser.parameters.forEach((parameter) => {
      parameter.build();
      argParser.cli.parameters[parameter.name] = parameter;
    });
  }
}

module.exports = {
  buildOptions,
  buildCommand,
  buildParameters,
};
