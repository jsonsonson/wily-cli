const createArgParser = require('./ArgParser.js');

module.exports = {

  on: (evt, func) => {
    return createArgParser().on(evt, func);
  },

  args: (args) => {
    return createArgParser().args(args);
  },

  description: (descriptionStr) => {
    return createArgParser().description(descriptionStr);
  },

  command: (command, description, action) => {
    return createArgParser().command(command, description, action);
  },

  defaultCommand: (command) => {
    return createArgParser().defaultCommand(command);
  },

  option: (option, description, options) => {
    return createArgParser().option(option, description, options);
  },

  parameter: (parameter, required, repeated) => {
    return createArgParser().parameter(parameter, required, repeated);
  },

  parameterSeparator: (separator) => {
    return createArgParser().parameterSeparator(separator);
  },

  version: (vers) => {
    return createArgParser().version(vers);
  },

  versionOption: (option, short) => {
    return createArgParser().versionOption(option, short);
  },

  usage: (usageStr) => {
    return createArgParser().usage(usageStr);
  },

  switchCharacter: (character) => {
    return createArgParser().switchCharacter(character);
  },

  enableGeneratedHelp: () => {
    return createArgParser().enableGeneratedHelp();
  },

  disableGeneratedHelp: () => {
    return createArgParser().disableGeneratedHelp();
  },

  helpOptionDescription: (description) => {
    return createArgParser().helpOptionDescription(description);
  },

  versionOptionDescription: (description) => {
    return createArgParser().versionOptionDescription(description);
  },

  helpOption: (option, short) => {
    return createArgParser().helpOption(option, short);
  },

  help: () => {
    return createArgParser().help();
  },

  helpPadding: (paddingStr) => {
    return createArgParser().helpPadding(paddingStr);
  },

  beforeHelpLog: (beforeHelpStr) => {
    return createArgParser().beforeHelpLog(beforeHelpStr);
  },

  afterHelpLog: (afterHelpStr) => {
    return createArgParser().afterHelpLog(afterHelpStr);
  },

  exitOnHelp: (exitHelp) => {
    return createArgParser().exitOnHelp(exitHelp);
  },

  exitOnVersion: (exitVersion) => {
    return createArgParser().exitOnVersion(exitVersion);
  },

  enableHelpUsage: () => {
    return createArgParser().enableHelpUsage();
  },

  enableHelpCommands: () => {
    return createArgParser().enableHelpCommands();
  },

  enableHelpOptions: () => {
    return createArgParser().enableHelpOptions();
  },

  enableHelpDescription: () => {
    return createArgParser().enableHelpDescription();
  },

  disableHelpUsage: () => {
    return createArgParser().disableHelpUsage();
  },

  disableHelpCommands: () => {
    return createArgParser().disableHelpCommands();
  },

  disableHelpOptions: () => {
    return createArgParser().disableHelpOptions();
  },

  disableHelpDescription: () => {
    return createArgParser().disableHelpDescription();
  },

  enableOptions: () => {
    return createArgParser().enableOptions();
  },

  enableCommands: () => {
    return createArgParser().enableCommands();
  },

  disableOptions: () => {
    return createArgParser().disableOptions();
  },

  disableCommands: () => {
    return createArgParser().disableCommands();
  },

  enableInteractivity: (prompt, quitCommand, quitCommandDesc) => {
    return createArgParser().enableInteractivity(prompt, quitCommand, quitCommandDesc);
  },

  disableInteractivity: () => {
    return createArgParser().disableInteractivity();
  },

  setInteractivityPrompt: (prompt) => {
    return createArgParser().setInteractivityPrompt(prompt);
  },

  setQuitInteractivityCommand: (command, description) => {
    return createArgParser().setQuitInteractivityCommand(command, description);
  },

  enableQuitInteractivityCommand: () => {
    return createArgParser().enableQuitInteractivityCommand();
  },

  disableQuitInteractivityCommand: () => {
    return createArgParser().disableQuitInteractivityCommand();
  },

  enableMissingRequiredMessage: () => {
    return createArgParser().enableMissingRequiredMessage();
  },

  disableMissingRequiredMessage: () => {
    return createArgParser().disableMissingRequiredMessage();
  },

  setMissingRequiredMessage: (message) => {
    return createArgParser().setMissingRequiredMessage(message);
  },

  onResult: (cb) => {
    return createArgParser().onResult(cb);
  },

  argParser: () => {
    return createArgParser();
  },

  parse: (argv) => {
    return createArgParser().parse(argv);
  },

  from: (config) => {
    return createArgParser().from(config);
  },
};
