const globalOptions = require('./globalOptions.js');

function getCommandInd(argParser, args) {
  return argParser.commandName ? args.indexOf(argParser.commandName) : argParser.commandInd;
}

function parseOptions(argParser) {
  let options = {};
  const customSwitchChar = argParser.cli.options._switchChar;
  const switchChar = customSwitchChar || '-';
  const commandInd = getCommandInd(argParser, argParser.argv);
  const extras = [];

  if (argParser.argv.length > commandInd) {
    const argParserCommands = argParser.commands.map(cmd => cmd.name);

    for (let i = commandInd; i < argParser.argv.length; i++) {
      const optionArg = argParser.argv[i];

      if (optionArg !== undefined && `${optionArg}`.indexOf(switchChar) === 0 && `${optionArg}`.length > 1) {
        let option = optionArg.substr(1);

        if (!customSwitchChar && option.indexOf(switchChar) === 0) {
          option = option.substr(1);
        }

        let value;

        let tempOption = option.replace(':', ' ').replace('=', ' ');
        const tempOptionSplit = tempOption.split(' ');
        tempOption = tempOptionSplit[0];

        const tempValue = option.replace(tempOption, '');
        if (tempValue) {
          value = tempValue.substr(1);
        }

        option = tempOption;

        if (argParser.cli.options._shorts[option]) {
          // Find long name if it's a short
          if (argParser.cli.options._shorts[option]) {
            option = argParser.cli.options._shorts[option];
          }
        }

        if (argParser.cli.options[option]) {
          while (i < argParser.argv.length - 1 && `${argParser.argv[i + 1]}`.indexOf(switchChar) !== 0 && !argParserCommands.includes(argParser.argv[i + 1])) {
            let arg = argParser.argv[i + 1];

            if (arg !== undefined && !isNaN(arg) && arg.length > 0) {
              arg = Number(arg);
            }

            if (value) {
              if (value.constructor !== Array) {
                value = [value];
              }

              value.push(arg);
            } else {
              value = arg;
            }

            i++; // skip next arg
          }

          if (argParser.cli.options[option].parameters !== undefined) {
            let val = {};

            if (value === undefined) {
              value = [];
            }

            if (value.constructor !== Array) {
              value = [value];
            }

            argParser.cli.options[option].parameters.forEach(param => {
              let paramValue = value.shift();

              if (paramValue !== undefined && !isNaN(paramValue) && `${paramValue}`.length > 0) {
                paramValue = Number(paramValue);
              }

              if (paramValue !== undefined) {
                if (val === undefined) {
                  val = {};
                }

                val[param] = paramValue;
              }
            });

            if (Object.keys(val).length === 0 && argParser.cli.options[option].default !== undefined) {
              val = argParser.cli.options[option].default;
            }

            value = val;
          }

          if (value === undefined) {
            value = argParser.cli.options[option].default !== undefined ? argParser.cli.options[option].default : true;
          }

          if ((value === false || value === true) && option.toLowerCase().substr(0, 3) === 'no-') {
            const oppositeOption = option.substr(3);
            options[oppositeOption] = !value;
          }

          options[option] = argParser.cli.options[option].modifier ? argParser.cli.options[option].modifier(value) : value;

          if (argParser.cli.options[option].isGlobal) {
            globalOptions.add(option, options[option]);
          }
        } else {
          extras.push(option);
        }
      }
    }
  }

  if (extras.length > 0) {
    options._extras = extras;
  }

  if (!options) {
    options = {};
  }

  return options;
}

function getSkipArgs(argParser) {
  const skipArgs = [];
  const commandInd = getCommandInd(argParser, argParser.argv);

  if (argParser.argv.length > commandInd) {
    const argParserCommands = argParser.commands.map(cmd => cmd.name);

    for (let i = commandInd; i < argParser.argv.length; i++) {
      const arg = argParser.argv[i];

      if (argParserCommands.includes(arg)) {
        break;
      }

      if (arg !== argParser.commandName) {
        skipArgs.push(arg);
      }
    }
  }

  return skipArgs;
}

function parseCommand(argParser) {
  const skipArgs = getSkipArgs(argParser);
  argParser.commandInd += skipArgs.length;

  argParser.commands.forEach(subCommand => {
    if (subCommand.constructor !== String) {
      const cmdAction = subCommand._action;

      if (cmdAction.constructor !== String) {
        cmdAction.commandInd = argParser.commandInd + 1;
      }
    }
  });

  let cmd = '';
  const parsed = argParser.argv[argParser.commandInd];

  const commandExists = argParser.commands.some(command => {
    command.buildParams();
    return command._n === parsed || command.name === parsed;
  });

  if (commandExists) {
    cmd = parsed;
  }

  argParser.numParamSeparators = undefined;

  return cmd;
}

function parseParameters(argParser) {
  let parameters = {};

  if (argParser.cli.parameters !== undefined) {
    const params = [];
    const subCommands = argParser.commands ? argParser.commands.map(command => {
      command.buildParams();

      if (command.name !== undefined && command.name !== argParser.commandName) {
        return command.name;
      }

      return undefined;
    }) : [];
    const commandInd = getCommandInd(argParser, argParser.argv);

    let argv = argParser.argv.slice(commandInd);
    let args = [];

    const paramSeparator = (argParser.paramSeparator || '').trim();
    const switchChar = argParser.cli.options._switchChar || '-';

    if (paramSeparator !== '') {
      argv.forEach(arg => {
        let a = arg;

        if (a !== paramSeparator && `${a}`.includes(paramSeparator)) {
          a = a.split(paramSeparator);
        } else {
          a = [a];
        }

        args.push(...a);
      });

      const tempArgs = [];
      let checkIsParam = paramSeparator !== '';

      for (let arg of args) {
        if (!subCommands.some(subCommand => subCommand.indexOf(arg) === 0)) {
          if (checkIsParam) {
            if (`${arg}`.trim() === paramSeparator) {
              checkIsParam = false;

              if (argParser.numParamSeparators === undefined) {
                argParser.numParamSeparators = 0;
              }

              argParser.numParamSeparators++;
            } else {
              tempArgs.push(arg);
            }
          } else {
            checkIsParam = true;
            tempArgs.push(arg);
          }
        } else {
          break;
        }
      }

      args = tempArgs;
    } else {
      args = argv;
    }

    for (let i = 0; i < args.length; i++) {
      const arg = args[i];

      if (`${arg}`.indexOf(switchChar) === 0 || subCommands.some(subCommand => subCommand.indexOf(arg) === 0)) {
        break;
      }

      if (argParser.commandName === undefined || argParser.commandName !== arg) {
        params.push(arg);
      }
    }

    const numArgParserParams = Object.keys(argParser.cli.parameters).length;
    let numRemainingParams = numArgParserParams;

    if (numRemainingParams > params.length) {
      numRemainingParams = params.length;
    }

    for (let i = 0; i < numArgParserParams && params.length > 0; i++) {
      const paramKey = Object.keys(argParser.cli.parameters)[i];
      const p = argParser.cli.parameters[paramKey];

      if (!p.repeated) {
        let value = params.shift();

        if (value !== undefined && !isNaN(value) && value.length > 0) {
          value = Number(value);
        }

        if (value !== undefined) {
          parameters[paramKey] = value;
        }

        numRemainingParams--;
      } else {
        while (params.length >= numRemainingParams) {
          let value = params.shift();

          if (value !== undefined && !isNaN(value) && value.length > 0) {
            value = Number(value);
          }

          if (value !== undefined) {
            if (parameters[paramKey] === undefined) {
              parameters[paramKey] = [];
            }

            parameters[paramKey].push(value);
          }
        }

        numRemainingParams--;
      }
    }

    if (params.length > 0) {
      parameters._extras = params;
    }
  }

  if (!parameters) {
    parameters = {};
  }

  return parameters;
}

module.exports = {
  parseOptions,
  parseCommand,
  parseParameters,
};
