const values = require('./values.js');

function getCallerFile() {
  const originalFunc = Error.prepareStackTrace;

  let callerfile;
  try {
    const err = new Error();
    Error.prepareStackTrace = function(err, stack) {
      if (!err) {
        return stack;
      }

      return stack;
    };

    callerfile = err.stack[3].getFileName();
  } catch (e) {}

  Error.prepareStackTrace = originalFunc;

  return callerfile;
}

function replaceColors(str) {
  let s = str.slice(0);

  if (s.includes('[') && s.includes(']')) {
    Object.keys(values.colors).forEach((color) => {
      const regex = new RegExp(`\\[${color}\\]`, 'g');
      s = s.replace(regex, values.colors[color]);
    });
  }

  return s;
}

function initialCommand(argParser) {
  if (argParser.interactive) {
    return '';
  }

  return argParser.isBinary ? process.argv0 : argParser.argv[1].split(process.platform === 'win32' ? '\\' : '/').pop();
}

function linkCommands(calledCommand, argParser) {
  const command = argParser.cli.commands[calledCommand];
  const subCommand = argParser.subCommands.find(subCmd => subCmd.commandName === calledCommand);

  if (command && subCommand) {
    if (!argParser.built) {
      subCommand.commandList.push(...argParser.commandList);
    }

    if (!subCommand.commandList.includes(calledCommand)) {
      subCommand.commandList.push(calledCommand);
    }
  }
}

function printVersion(argParser) {
  console.log(`${argParser.vers}${values.colors.clear}`);
}

function setInteractivity(argParser) {
  if (!argParser.built && argParser.interactive) {
    argParser.subCommands.forEach(subCommand => {
      subCommand.interactive = true;
    });
  }
}

function formatStringToConsole(str, padding) {
  const colorsRegex = new RegExp('\\\u001b\\[\\d+m', 'g');
  let string = `${str}`.replace('\t', '        ');
  const stringArray = string.split(' ');
  let paddingString = `${padding}`.replace('\t', '        ');

  let match;
  while (match = colorsRegex.exec(padding)) {
    paddingString = paddingString.replace(match[0], '');
  }

  const pad = Array(paddingString.length + 1).join(' ');
  const consoleWidth = process.stdout.columns;
  let currentLength = pad.length;
  const newStringArray = [];

  stringArray.forEach((str, index) => {
    let s = `${str}`;

    while (match = colorsRegex.exec(str)) {
      s = s.replace(match[0], '');
    }

    currentLength += s.length + (index < stringArray.length - 1 ? 1 : 0);

    if (currentLength >= consoleWidth) {
      // TODO split str if it in itself exceeds the console width
      newStringArray.push(str);
      currentLength = pad.length + str.length;
    } else {
      const ind = newStringArray.length - 1;

      if (ind < 0) {
        newStringArray.push(str);
      } else {
        newStringArray[ind] = `${newStringArray[ind]} ${str}`;
      }
    }
  });

  string = newStringArray.join(`\n${pad}`);

  return string;
}

function isMissingRequired(argParser, options, parameters, command) {
  const optionKeys = Object.keys(options).filter(key => key !== '_extras');
  const missingOptions = [];
  const missingParameters = [];

  if (argParser.missingRequiredEnabled &&
      !(argParser.commandsEnabled && argParser.interactive && command === argParser.quitCommand) &&
      !(argParser.optionsEnabled && optionKeys.length === 1 && (options[argParser.helpOpt] || options[argParser.versionOpt]))) {
    const foundParams = Object.keys(parameters);

    const cliOptions = argParser.cli.options;
    const cliParams = argParser.cli.parameters;

    const _missingOptions = argParser.optionsEnabled ? Object.keys(argParser.cli.options || [])
      .filter(option => cliOptions[option].required && !optionKeys.includes(option))
      .map(missingOption => {
        missingOptions.push(missingOption);
        return `${argParser.cli.options._switchChar || '--'}${missingOption}`;
      }) : [];

    const _missingParameters = argParser.commandsEnabled ? Object.keys(argParser.cli.parameters || [])
      .filter(param => cliParams[param].required && !foundParams.includes(param))
      .map(missingParam => {
        missingParameters.push(missingParam);
        return `<${missingParam}${cliParams[missingParam].repeated ? '...' : ''}>`;
      }) : [];

    const missing = [..._missingParameters, ..._missingOptions];

    if (missing.length > 0) {
      console.log(`${argParser.missingRequiredMessage}${values.colors.clear}${missing.join(' ')}`);
      console.log(`See '${argParser.cli.options._switchChar || '--'}${argParser.helpOpt}'${values.colors.clear}`);
    }
  }

  return { missingOptions, missingParameters };
}

function executeResultCallback(argParser, result) {
  if (argParser.resultCallback !== undefined && argParser.resultCallback.constructor === Function) {
    if (argParser.resultCallback.isPromise) {
      argParser.waitingForResultPromise = true;

      argParser.resultCallback(result)
        .then(() => {
          argParser.resultPromiseComplete = true;
        })
        .catch(err => {
          argParser.resultPromiseComplete = true;

          if (argParser.onError !== undefined && argParser.onError.constructor === Function) {
            argParser.onError(err);
          }
        });
    } else {
      argParser.resultCallback(result);
    }
  }
}

module.exports = {
  getCallerFile,
  replaceColors,
  initialCommand,
  linkCommands,
  printVersion,
  setInteractivity,
  formatStringToConsole,
  isMissingRequired,
  executeResultCallback,
};
