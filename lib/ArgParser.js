const events = require('events');
const builders = require('./builders.js');
const Option = require('./Option.js');
const Parameter = require('./Parameter.js');
const Command = require('./Command.js');
const values = require('./values.js');
const parsers = require('./parsers.js');
const helpers = require('./helpers.js');
const globalOptions = require('./globalOptions.js');

class ArgParser {

  constructor() {
    this.eventEmitter = new events.EventEmitter();
    // the commands executed to reach this command
    this.commandList = [];
    // list of sub commands for this command
    this.subCommands = [];
    // argv index of this command (default is 2 - command function sets this for subcommands)
    this.isBinary = process.argv0 !== 'node';
    this.commandInd = 2;
    this.argv = process.argv;
    this.commandName = undefined;
    this.options = [];
    this.parameters = [];
    this.commands = [];
    this.commandsHelpEnabled = true;
    this.usageHelpEnabled = true;
    this.optionsHelpEnabled = true;
    this.descriptionHelpEnabled = true;
    this.commandsEnabled = true;
    this.optionsEnabled = true;
    this.built = false;
    this.quitCommand = 'quit';
    this.quitCommandDesc = 'Quit the CLI';
    this.quitEnabled = true;
    this.isFirstCommand = true;
    this.helpPaddingStr = '\t';
    this.result = {};
    this.generateHelp = true;
    this.paramSeparator = ' ';
    this.versionOpt = 'version';
    this.versionOptShort = 'v';
    this.helpOpt = 'help';
    this.helpOptShort = 'h';
    this.missingRequiredEnabled = true;
    this.missingRequiredMessage = `${values.colors.red}error${values.colors.clear} Missing required argument(s): `;
    this.exitMissingRequired = true;
    this.onError = (err) => {
      throw err;
    };
    this.cli = {
      commands: {},
      parameters: {},
      options: {
        _shorts: {},
      },
    };

    if (!process.env.DISABLE_PARSE) {
      process.on('beforeExit', () => {
        if (this.isFirstCommand && !this.interactive && !this.parsed && this.commandName === undefined) {
          this.parse();
        }
      });

      process.on('exit', () => {
        if (this.isFirstCommand && !this.interactive && !this.parsed && this.commandName === undefined) {
          this.parse();
        }
      });
    }

    this.option(this.helpOpt, 'Output usage information');

    if (process.env.params && process.env.params.length > 0) {
      const params = JSON.parse(process.env.params);

      for (let param in params) {
        const p = params[param];
        new Parameter(p.name, p.required, p.repeated, this).build();
      };
    }

    if (process.env.globalOptions && process.env.globalOptions.length > 0) {
      const globalOpts = JSON.parse(process.env.globalOptions);
      globalOptions.set(globalOpts);
    }
  }

  parse(argv) {
    this.parsed = true;

    try {
      this.result = {};
      this.args(argv);

      if (!this.builtVersion && this.vers !== undefined && this.versionOpt !== undefined) {
        this.builtVersion = true;
		    const options = {};

        if (this.versionOptShort !== undefined && this.versionOptShort !== 'v') {
          options.short = this.versionOptShort;
        }

        let versionDesc = 'Output version';

        if (this.versionDescription !== undefined) {
          versionDesc = this.versionDescription;
        }

        this.option(this.versionOpt, versionDesc, options);
      }

      if (!this.built && this.interactive && !this.commandName && this.quitEnabled) {
        // Only add exit command to top level command
        this.command(this.quitCommand, this.quitCommandDesc, () => {
          this.eventEmitter.emit('quit', options, parameters, command);
          process.exit(0);
        });
      }

      helpers.setInteractivity(this);

      this.oldCommandInd = this.commandInd;

      builders.buildOptions(this);
      builders.buildParameters(this);

      let parameters = this.commandsEnabled ? parsers.parseParameters(this) : {};

      if (!parameters) {
        parameters = {};
      }

      let options = this.optionsEnabled ? parsers.parseOptions(this) : {};

      const globalOpts = globalOptions.get();
      Object.keys(globalOpts).forEach(globalOption => {
        if (options && options[globalOption] === undefined) {
          options[globalOption] = globalOpts[globalOption];
        }
      });

      if (!options) {
        options = {};
      }

      let command;
      if (this.commandsEnabled) {
        const parsed = parsers.parseCommand(this);
        command = parsed !== undefined && parsed.length > 0 ? parsed : this.defCommand;

        if ((parsed === undefined || parsed.length === 0) && command !== undefined && command === this.defCommand) {
          // Insert default command to satisfy parsing sub command paramter parsing
          this.argv.splice(this.oldCommandInd, 0, command);
        }
      }

      this.commandInd = this.oldCommandInd;
      builders.buildCommand(this, command);
      helpers.linkCommands(command, this);
      const optionKeys = Object.keys(options).filter(key => key !== '_extras');
      this.built = true;

      this.eventEmitter.emit('before', options, parameters, command);

      let missingOptions = [];
      let missingParameters = [];
      const missing = helpers.isMissingRequired(this, options, parameters, command) || {};
      missingOptions = missing.missingOptions || [];
      missingParameters = missing.missingParameters || [];
      const missingArgs = missingOptions.length + missingParameters.length > 0;
      let subCommandParameters = {};

      if (!missingArgs) {
        if (this.cli.commands[command] === undefined && optionKeys.length === 1 && (options[this.helpOpt] || options[this.versionOpt])) {
          if (this.optionsEnabled) {
            if (options[this.helpOpt]) {
              this.help();
              this.eventEmitter.emit('help', options, parameters, command);

              if (this.exitHelp) {
                process.exit(0);
              }
            } else if (options[this.versionOpt]) {
              helpers.printVersion(this);
              this.eventEmitter.emit('version', options, parameters, command);

              if (this.exitVersion) {
                process.exit(0);
              }
            }
          }
        } else {
          this.eventEmitter.emit('exec', options, parameters, command);

          optionKeys.forEach(option => {
            if (option !== 'help' && option !== 'version') {
              this.eventEmitter.emit(option, options, parameters, command);
            }
          });

          Object.keys(parameters).forEach(parameter => {
            this.eventEmitter.emit(parameter, options, parameters, command);
          });

          if (command !== undefined && this.cli.commands[command] !== undefined) {
            const action = this.cli.commands[command].action;

            if (action !== undefined && action.constructor === Function) {
              const subCmdAction = this.subCommands.find(subCmd => subCmd.commandName === command);

              if (subCmdAction !== undefined) {
                if (!subCmdAction.argv) {
                  subCmdAction.argv = this.argv;
                }

                if (!subCmdAction.subCommands) {
                  subCmdAction.subCommands = [];
                }

                if (!subCmdAction.cli) {
                  subCmdAction.cli = {};
                }

                if (!subCmdAction.cli.options) {
                  subCmdAction.cli.options = {
                    _shorts: {},
                    _defaults: {},
                  };
                }
              }

              if (command !== this.defCommand) {
                if (subCmdAction !== undefined) {
                  subCmdAction.argv = this.argv;
                  subCommandParameters = parsers.parseParameters(subCmdAction) || {};
                  const subCmdMissing = helpers.isMissingRequired(subCmdAction, options, subCommandParameters, command);
                  missingOptions = subCmdMissing.missingOptions;
                  missingParameters = subCmdMissing.missingParameters;
                  const subCmdMissingArgs = missingOptions.length + missingParameters.length > 0;

                  if (!subCmdMissingArgs) {
                    action(options, subCommandParameters, command);
                  }
                } else {
                  // Binary action
                  action();
                }
              } else {
                if (this.optionsEnabled && optionKeys.length === 1 && (options[this.helpOpt] || options[this.versionOpt])) {
                  if (options[this.helpOpt]) {
                    this.help();
                    this.eventEmitter.emit('help', options, parameters, command);

                    if (this.exitHelp) {
                      process.exit(0);
                    }
                  } else if (options[this.versionOpt]) {
                    helpers.printVersion(this);
                    this.eventEmitter.emit('version', options, parameters, command);

                    if (this.exitVersion) {
                      process.exit(0);
                    }
                  }
                } else {
                  if (subCmdAction !== undefined) {
                    subCmdAction.argv = this.argv;
                    subCommandParameters = parsers.parseParameters(subCmdAction) || {};
                    const subCmdMissing = helpers.isMissingRequired(subCmdAction, options, subCommandParameters, command);
                    missingOptions = subCmdMissing.missingOptions;
                    missingParameters = subCmdMissing.missingParameters;
                    const subCmdMissingArgs = missingOptions.length + missingParameters.length > 0;

                    if (!subCmdMissingArgs) {
                      action(options, subCommandParameters, command);
                    }
                  } else {
                    // Binary action
                    action();
                  }
                }
              }
            }

            this.eventEmitter.emit('command', options, parameters, command);
          } else if (this.optionsEnabled && optionKeys.length === 0 && Object.keys(parameters).length === 0 && (this.commandName === undefined || this.commandName !== command) && this.eventEmitter.listenerCount('exec') === 0) {
            // An undefined commandName means this is the initial command
            // We don't want to automatically display help if the provided other options
            options[this.helpOpt] = true;
            this.help();
            this.eventEmitter.emit('help', options, parameters, command);

            if (this.exitHelp) {
              process.exit(0);
            }
          }
        }
      }

      this.eventEmitter.emit('after', options, parameters, command);

      const result = {
        command,
        options,
        parameters,
        subCommandParameters,
        missingParameters,
        missingOptions,
      };

      helpers.executeResultCallback(this, result);

      return result;
    } catch (err) {
      if (this.onError !== undefined && this.onError.constructor === Function) {
        this.onError(err);
      }

      return {};
    }
  }

  on(evt, func) {
    let eventFunc = func;

    if (evt && evt.constructor === String && func &&
      (func.constructor === Function || func.constructor.name === 'AsyncFunction' || func.constructor.name === 'Promise')) {
      if (func.constructor.name === 'AsyncFunction' || func.constructor.name === 'Promise') {
        eventFunc = (options, parameters, command) => {
          if (this.numEventPromises === undefined) {
            this.numEventPromises = 0;
          }

          this.numEventPromises++;

          (func.constructor.name === 'AsyncFunction' ? func(options, parameters, command) : func)
            .then(() => {
              this.numEventPromises--;
            })
            .catch(err => {
              this.numEventPromises--;

              if (this.onError !== undefined && this.onError.constructor === Function) {
                this.onError(err);
              }
            });
        };
      }

      if (evt === 'error') {
        this.onError = eventFunc;
      } else {
        this.eventEmitter.on(evt, eventFunc);
      }
    }

    return this;
  }

  args(args) {
    if (args && args.constructor === Array) {
      this.argv = args;
    }

    return this;
  }

  description(descriptionStr) {
    if (descriptionStr && descriptionStr.constructor === String && descriptionStr.length > 0) {
      const descStr = `${descriptionStr}`;
      this.descriptionStr = `${helpers.replaceColors(descStr)}${values.colors.clear}`;
    }

    return this;
  }

  command(command, description, action) {
    let desc = `${description}`;
    desc = `${helpers.replaceColors(desc)}${values.colors.clear}`;

    this.commands.push(new Command(command, desc, action, this));
    return this;
  }

  defaultCommand(command) {
    if (command !== undefined && command.constructor === String && command.length > 0) {
      this.defCommand = command;
    }

    return this;
  }

  option(option, description, options) {
    let desc = `${description}`;
    desc = `${helpers.replaceColors(desc)}${values.colors.clear}`;

    this.options.push(new Option(option, desc, this, options));
    return this;
  }

  parameter(parameter, required, repeated) {
    this.parameters.push(new Parameter(parameter, required, repeated, this));
    return this;
  }

  parameterSeparator(separator) {
    if (separator !== undefined && `${separator}`.length > 0) {
      this.paramSeparator = `${separator}`;
    }

    return this;
  }

  version(vers) {
    // version must be a string or number
    if (vers && (vers.constructor === String && vers.length > 0 || vers.constructor === Number)) {
      const v = `${vers}`;
      this.vers = `${helpers.replaceColors(v)}${values.colors.clear}`;
    }

    return this;
  }

  versionOption(option, short) {
    if (option !== undefined && `${option}`.length > 0) {
      if (short !== undefined && `${short}`.length > 0) {
        this.versionOptShort = short;
      }

      this.versionOpt = option;
    }

    return this;
  }

  usage(usageStr) {
    if (usageStr && usageStr.constructor === String && usageStr.length > 0) {
      this.usageStr = `${helpers.replaceColors(usageStr)}${values.colors.clear}`;
    }

    return this;
  }

  switchCharacter(character) {
    if (character && character.constructor === String && character.length === 1) {
      this.cli.options._switchChar = character;
    }

    return this;
  }

  enableGeneratedHelp() {
    this.generateHelp = true;
    return this;
  }

  disableGeneratedHelp() {
    this.generateHelp = false;
    return this;
  }

  helpOptionDescription(description) {
    if (description !== undefined && `${description}`.length > 0) {
      const helpOption = this.options.find(option => option._n === this.helpOpt);

      if (helpOption !== undefined) {
        let desc = `${description}`;
        desc = `${helpers.replaceColors(desc)}${values.colors.clear}`;

        helpOption.description = desc;
      }
    }

    return this;
  }

  helpOption(option, short) {
    if (option !== undefined && `${option}`.length > 0) {
      const helpOption = this.options.find(option => option._n === this.helpOpt);

      if (helpOption !== undefined) {
        helpOption._n = option;
        this.helpOpt = option;
      }

      if (short !== undefined && `${short}`.length > 0) {
        if (helpOption.config === undefined) {
          helpOption.config = {};
        }

        helpOption.config.short = short;
        this.helpOptShort = short;
      }
    }

    return this;
  }

  versionOptionDescription(description) {
    if (description !== undefined && `${description}`.length > 0) {
      let desc = `${description}`;
      desc = `${helpers.replaceColors(desc)}${values.colors.clear}`;

      this.versionDescription = desc;
    }

    return this;
  }

  help() {
    if (this.generateHelp) {
      const switchChar = this.cli.options._switchChar;
      let program = `${process.env.initialCommand || helpers.initialCommand(this)}`.trim();

      if (process.env.previousCommands) {
        this.commandList.unshift(...process.env.previousCommands.split('{[~~]}'));
      }

      if (this.commandList.length > 0) {
        program = `${program} ${this.commandList.join(' ')}`.trim();
      }

      if (program || this.interactive) {
        const hasCommands = this.commands.length > 0; //Object.keys(this.cli.commands).length > 0;
        const hasParameters = Object.keys(this.cli.parameters).length > 0;
        let usageLine;

        if (this.usageHelpEnabled && this.usageStr) {
          const usageLineBefore = `${values.colors.clear}${this.helpPaddingStr}Usage: `;
          const usageLineAfter = helpers.formatStringToConsole(`${values.colors.yellow}${this.usageStr}${values.colors.clear}`, usageLineBefore);
          usageLine = `${usageLineBefore}${usageLineAfter}`;
        } else {
          const usageLineBefore = `${values.colors.clear}${this.helpPaddingStr}Usage: `;
          let usageLineAfter = '';

          if (program && program.length > 0) {
            usageLineAfter = `${usageLineAfter}${values.colors.yellow} ${program}${values.colors.clear}`;
          }

          if (hasParameters) {
            const parameters = [];

            const params = this.cli.parameters;
            const parameterKeys = Object.keys(params);

            parameterKeys.forEach(parameter => {
              parameters.push(params[parameter].help());
            });

            usageLineAfter = `${usageLineAfter} ${parameters.join(this.paramSeparator)}`;
          }

          usageLineAfter = `${usageLineAfter}${values.colors.cyan}`;

          if (hasCommands) {
            usageLineAfter = `${usageLineAfter} [command]`;
          }

          usageLineAfter = `${usageLineAfter} [options]${values.colors.clear}`;
          usageLineAfter = helpers.formatStringToConsole(usageLineAfter, usageLineBefore);
          usageLine = `${usageLineBefore}${usageLineAfter}`;
        }

        if (this.usageHelpEnabled || this.optionsEnabled && this.optionsHelpEnabled || this.commandsEnabled && this.commandsHelpEnabled || this.descriptionHelpEnabled) {
          console.log();
        }

        if (this.beforeHelpStr) {
          console.log(`${this.beforeHelpStr}${values.colors.clear}`);
          console.log();
        }

        if (this.descriptionHelpEnabled && this.descriptionStr) {
          const descriptionBefore = `${this.helpPaddingStr}`;
          const descriptionAfter = helpers.formatStringToConsole(`${this.descriptionStr}${values.colors.clear}`, descriptionBefore);
          console.log(`${descriptionBefore}${descriptionAfter}`);
          console.log();
        }

        if (this.usageHelpEnabled) {
          console.log(usageLine);
        }

        if (this.commandsEnabled && this.commandsHelpEnabled) {
          if (hasCommands) {
            if (this.usageHelpEnabled || this.descriptionHelpEnabled) {
              console.log();
            }

            console.log(`${this.helpPaddingStr}Commands:`);

            const commands = this.commands;
            // const commandKeys = Object.keys(commands);
            let longestCommand = 0;

            commands.forEach(command => {
              if (command.constructor === Command) {
                let cmdStr = command.toString();

                if (cmdStr.length > longestCommand) {
                  longestCommand = cmdStr.length;
                }
              }
            });

            longestCommand += 1;

            commands.forEach(command => {
              if (command.constructor === Command) {
                console.log(command.help(longestCommand));
              }
            });
          }
        }

        if (this.optionsEnabled && this.optionsHelpEnabled) {
          if (this.usageHelpEnabled || this.descriptionHelpEnabled || this.commandsEnabled && this.commandsHelpEnabled) {
            console.log();
          }

          console.log(`${this.helpPaddingStr}Options:`);

          const options = this.cli.options;
          const optionKeys = Object.keys(options);
          let longestOption = 0;

          optionKeys.forEach(option => {
            const opt = options[option];

            if (opt.constructor === Option) {
              let optionStr = opt.toString(switchChar);

              if (optionStr.length > longestOption) {
                longestOption = optionStr.length;
              }
            }
          });

          longestOption += 1;

          optionKeys.forEach(option => {
            const opt = options[option];

            if (opt.constructor === Option) {
              const optionHelp = opt.help(longestOption, switchChar);
              console.log(optionHelp);
            }
          });
        }

        if (this.usageHelpEnabled || this.optionsEnabled && this.optionsHelpEnabled || this.commandsEnabled && this.commandsHelpEnabled || this.descriptionHelpEnabled) {
          console.log();
        }

        if (this.afterHelpStr) {
          console.log(`${this.afterHelpStr}${values.colors.clear}`);
        }
      }
    }

    return this;
  }

  helpPadding(paddingStr) {
    if (paddingStr !== undefined) {
      const padStr = `${paddingStr}`;
      this.helpPaddingStr = `${helpers.replaceColors(padStr)}${values.colors.clear}`;
    }

    return this;
  }

  beforeHelpLog(beforeHelpStr) {
    if (beforeHelpStr && beforeHelpStr.constructor === String && beforeHelpStr.length > 0) {
      this.beforeHelpStr = `${helpers.replaceColors(beforeHelpStr)}${values.colors.clear}`;
    }

    return this;
  }

  afterHelpLog(afterHelpStr) {
    if (afterHelpStr && afterHelpStr.constructor === String && afterHelpStr.length > 0) {
      this.afterHelpStr = `${helpers.replaceColors(afterHelpStr)}${values.colors.clear}`;
    }

    return this;
  }

  exitOnHelp(exitHelp) {
    this.exitHelp = exitHelp;
    return this;
  }

  exitOnVersion(exitVersion) {
    this.exitVersion = exitVersion;
    return this;
  }

  enableHelpUsage() {
    this.usageHelpEnabled = true;
    return this;
  }

  enableHelpCommands() {
    this.commandsHelpEnabled = true;
    return this;
  }

  enableHelpOptions() {
    this.optionsHelpEnabled = true;
    return this;
  }

  enableHelpDescription() {
    this.descriptionHelpEnabled = true;
    return this;
  }

  disableHelpUsage() {
    this.usageHelpEnabled = false;
    return this;
  }

  disableHelpCommands() {
    this.commandsHelpEnabled = false;
    return this;
  }

  disableHelpOptions() {
    this.optionsHelpEnabled = false;
    return this;
  }

  disableHelpDescription() {
    this.descriptionHelpEnabled = false;
    return this;
  }

  enableOptions() {
    this.optionsEnabled = true;
    return this;
  }

  enableCommands() {
    this.commandsEnabled = true;
    return this;
  }

  disableOptions() {
    this.optionsEnabled = false;
    return this;
  }

  disableCommands() {
    this.commandsEnabled = false;
    return this;
  }

  enableInteractivity(prompt, quitCommand, quitCommandDesc) {
    if (!this.interactive) {
      this.commandInd -= 2;
    }

    this.interactive = true;

    if (prompt !== undefined) {
      this.setInteractivityPrompt(prompt);
    }

    if (quitCommand !== undefined) {
      this.setQuitInteractivityCommand(quitCommand, quitCommandDesc);
    }

    if (!process.env.DISABLE_PARSE) {
      if (process.stdin.setRawMode !== undefined) {
        process.stdin.setRawMode(true);
        process.stdin.resume();
        process.stdin.setEncoding('utf8');

        const readline = require('readline');
        const rl = readline.createInterface({
          input: process.stdin,
          output: process.stdout,
        });

        rl.setPrompt(this.prompt || '');

        process.stdin.on('resume', () => {
          if (this.interactive) {
            rl.prompt();
          } else {
            rl.pause();
            rl.close();
            process.stdin.pause();
          }
        });

        rl.on('line', input => {
          if (this.interactive) {
            let cmd = input.trim();
            cmd = cmd.replace(/\r/g, '').replace(/\n/g, '');
            this.parse(cmd.split(' '));

            globalOptions.clear();
            const waitingForPromise = this.waitingForCommandPromise ||
              this.waitingForResultPromise ||
              this.numEventPromises > 0;

            if (!waitingForPromise) {
              rl.prompt();
            } else {
              const checkPromise = () => {
                const commandPromiseComplete = this.waitingForCommandPromise ? this.commandPromiseComplete : true;
                const resultPromiseComplete = this.waitingForResultPromise ? this.resultPromiseComplete : true;
                const eventPromisesComplete = this.numEventPromises === 0;

                const promisesComplete = commandPromiseComplete && resultPromiseComplete && eventPromisesComplete;

                if (!promisesComplete) {
                  setTimeout(checkPromise, 10);
                } else {
                  delete this.waitingForCommandPromise;
                  delete this.commandPromiseComplete;
                  delete this.waitingForResultPromise;
                  delete this.resultPromiseComplete;
                  delete this.numEventPromises;

                  rl.prompt();
                }
              };

              checkPromise();
            }
          } else {
            rl.pause();
            rl.close();
            process.stdin.pause();
          }
        });

        process.stdin.on('data', function(key) {
          if (key === '\u0003') {
            console.log();
            process.exit();
          }
        });
      } else {
        // revert back to old sad way
        process.stdin.on('readable', () => {
          if (this.interactive) {
            if (this.prompt !== undefined && this.prompt.length > 0) {
              process.stdout.write(`${this.prompt}`);
            }
          } else {
            process.stdin.resume();
            process.stdin.read();
            process.stdin.pause();
          }
        });

        process.stdin.setEncoding('utf8');
        process.stdin.on('data', chunk => {
          if (this.interactive) {
            let cmd = chunk.toString().trim();
            cmd = cmd.replace(/\r/g, '').replace(/\n/g, '');
            this.result = this.parse(cmd.split(' '));

            if (this.resultCallback !== undefined && this.resultCallback.constructor === Function) {
              helpers.executeResultCallback(this, result);
            }
          } else {
            process.stdin.resume();
            process.stdin.read();
            process.stdin.pause();
          }
        });
      }
    }

    return this;
  }

  disableInteractivity() {
    if (this.interactive) {
      this.commandInd += 2;
    }

    this.interactive = false;
    process.stdin.pause();
    return this;
  }

  setInteractivityPrompt(prompt) {
    if (prompt !== undefined && `${prompt}`.length > 0) {
      const prompStr = `${prompt}`;
      this.prompt = `${helpers.replaceColors(prompStr)}${values.colors.clear}`;
    }

    return this;
  }

  setQuitInteractivityCommand(command, description) {
    if (command !== undefined && `${command}`.length > 0) {
      const commandStr = `${command}`;
      const descriptionStr = description !== undefined && `${description}`.length > 0 ? `${description}` : 'Quit the CLI';

      this.quitCommand = commandStr;
      this.quitCommandDesc = descriptionStr;
    }

    return this;
  }

  enableQuitInteractivityCommand() {
    this.quitEnabled = true;
    return this;
  }

  disableQuitInteractivityCommand() {
    this.quitEnabled = false;
    return this;
  }

  onResult(cb) {
    this.resultCallback = cb;

    if (this.resultCallback !== undefined &&
      (this.resultCallback.constructor.name === 'AsyncFunction' || this.resultCallback.constructor.name === 'Promise')) {
      const asyncResult = this.resultCallback;

      this.resultCallback = (results) => {
        return asyncResult.constructor.name === 'Promise' ?
          asyncResult : new Promise((resolve, reject) => {
            asyncResult(results)
              .then(resolved => {
                resolve(resolved);
              })
              .catch(err => {
                reject(err);
              });
          });
      };

      this.resultCallback.isPromise = true;
    }

    return this;
  }

  enableMissingRequiredMessage() {
    this.missingRequiredEnabled = true;
    return this;
  }

  disableMissingRequiredMessage() {
    this.missingRequiredEnabled = false;
    return this;
  }

  setMissingRequiredMessage(message) {
    if (message !== undefined && `${message}`.length > 0) {
      const messageStr = `${message}`;
      this.missingRequiredMessage = `${helpers.replaceColors(messageStr)}${values.colors.clear}`;
    }

    return this;
  }

  from(config) {
    if (config.commands && config.commands.constructor === Array && config.commands.length > 0) {
      config.commands.forEach(commandArr => {
        this.command(commandArr[0] || '', commandArr[1] || '', commandArr[2] || undefined);
      });
    }

    if (config.options && config.options.constructor === Array && config.options.length > 0) {
      config.options.forEach(optionArr => {
        this.option(optionArr[0] || '', optionArr[1] || '', optionArr[2] || {});
      });
    }

    if (config.parameters && config.parameters.constructor === Array && config.parameters.length > 0) {
      config.parameters.forEach(parameterArr => {
        this.parameter(parameterArr[0] || '', parameterArr[1] || false, parameterArr[2] || false);
      });
    }

    if (config.events && config.events.constructor === Array && config.events.length > 0) {
      config.events.forEach(eventArr => {
        this.on(eventArr[0] || '', eventArr[1] || undefined);
      });
    }

    if (config.args && config.args.constructor === Array && config.args.length > 0) {
      this.args(config.args);
    }

    if (config.description !== undefined) {
      this.description(config.description);
    }

    if (config.defaultCommand !== undefined) {
      this.defaultCommand(config.defaultCommand);
    }

    if (config.parameterSeparator !== undefined) {
      this.parameterSeparator(config.parameterSeparator);
    }

    if (config.version !== undefined) {
      this.version(config.version);
    }

    if (config.versionOption && config.versionOption.constructor === Array && config.versionOption.length > 0) {
      this.versionOption(config.versionOption[0] || undefined, config.versionOption[1] || undefined);
    }

    if (config.usage !== undefined) {
      this.usage(config.usage || undefined);
    }

    if (config.switchCharacter !== undefined) {
      this.switchCharacter(config.switchCharacter || undefined);
    }

    if (config.generateHelpEnabled !== undefined && config.generateHelpEnabled.constructor === Boolean) {
      if (config.generateHelpEnabled) {
        this.enableGeneratedHelp();
      } else {
        this.disableGeneratedHelp();
      }
    }

    if (config.helpOptionDescription !== undefined) {
      this.helpOptionDescription(config.helpOptionDescription || undefined);
    }

    if (config.helpOption && config.helpOption.constructor === Array && config.helpOption.length > 0) {
      this.helpOption(config.helpOption[0] || undefined, config.helpOption[1] || undefined);
    }

    if (config.versionOptionDescription !== undefined) {
      this.versionOptionDescription(config.versionOptionDescription || undefined);
    }

    if (config.helpPadding !== undefined) {
      this.helpPadding(config.helpPadding || undefined);
    }

    if (config.beforeHelpLog !== undefined) {
      this.beforeHelpLog(config.beforeHelpLog || undefined);
    }

    if (config.afterHelpLog !== undefined) {
      this.afterHelpLog(config.afterHelpLog || undefined);
    }

    if (config.exitOnHelp !== undefined && config.exitOnHelp.constructor === Boolean) {
      this.exitOnHelp(config.exitOnHelp);
    }

    if (config.exitOnVersion !== undefined && config.exitOnVersion.constructor === Boolean) {
      this.exitOnVersion(config.exitOnVersion);
    }

    if (config.helpUsageEnabled !== undefined && config.helpUsageEnabled.constructor === Boolean) {
      if (config.helpUsageEnabled) {
        this.enableHelpUsage();
      } else {
        this.disableHelpUsage();
      }
    }

    if (config.helpCommandsEnabled !== undefined && config.helpCommandsEnabled.constructor === Boolean) {
      if (config.helpCommandsEnabled) {
        this.enableHelpCommands();
      } else {
        this.disableHelpCommands();
      }
    }

    if (config.helpOptionsEnabled !== undefined && config.helpOptionsEnabled.constructor === Boolean) {
      if (config.helpOptionsEnabled) {
        this.enableHelpOptions();
      } else {
        this.disableHelpOptions();
      }
    }

    if (config.helpDescriptionEnabled !== undefined && config.helpDescriptionEnabled.constructor === Boolean) {
      if (config.helpDescriptionEnabled) {
        this.enableHelpDescription();
      } else {
        this.disableHelpDescription();
      }
    }

    if (config.optionsEnabled !== undefined && config.optionsEnabled.constructor === Boolean) {
      if (config.optionsEnabled) {
        this.enableOptions();
      } else {
        this.disableOptions();
      }
    }

    if (config.commandsEnabled !== undefined && config.commandsEnabled.constructor === Boolean) {
      if (config.commandsEnabled) {
        this.enableCommands();
      } else {
        this.disableCommands();
      }
    }

    if (config.interactive && config.interactive.constructor === Object && Object.keys(config.interactive).length > 0) {
      this.enableInteractivity(config.interactive.prompt || undefined, config.interactive.quitCommand || undefined, config.interactive.quitCommandDescription || undefined);

      if (config.interactive.quitCommandEnabled !== undefined && config.interactive.quitCommandEnabled.constructor === Boolean) {
        if (config.interactive.quitCommandEnabled) {
          this.enableQuitInteractivityCommand();
        } else {
          this.disableQuitInteractivityCommand();
        }
      }
    }

    if (config.onResult !== undefined && config.onResult.constructor === Function) {
      this.onResult(config.onResult);
    }

    if (config.missingRequiredMessageEnabled !== undefined && config.missingRequiredMessageEnabled.constructor === Boolean) {
      if (config.missingRequiredMessageEnabled) {
        this.enableMissingRequiredMessage();
      } else {
        this.disableMissingRequiredMessage();
      }
    }

    if (config.missingRequiredMessage !== undefined) {
      this.setMissingRequiredMessage(config.missingRequiredMessage);
    }

    return this;
  }
}

function createArgParser() {
  const callerFile = helpers.getCallerFile();
  const argParser = new ArgParser();
  argParser.callerFile = callerFile;
  return argParser;
}

module.exports = createArgParser;
