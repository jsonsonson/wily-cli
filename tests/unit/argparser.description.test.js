const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should set the description', () => {
      const description = 'abc';
      argParser.description(description);

      expect(argParser.descriptionStr.includes(description)).to.equal(true);
    });
  });
});
