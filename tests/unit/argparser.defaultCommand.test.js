const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should set the default command', () => {
      const cmdName = 'command12234';
      argParser
        .command(cmdName, 'Function', () => {})
        .defaultCommand(cmdName);

      expect(argParser.defCommand).to.equal(cmdName);
    });
  });
});
