const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should set the help option description', () => {
      const description = 'abc';
      argParser.helpOptionDescription(description);

      const helpOption = argParser.options.find(option => option._n === 'help');

      expect(helpOption.description.includes(description)).to.equal(true);
    });
  });
});
