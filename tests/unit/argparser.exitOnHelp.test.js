const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should set exit on help', () => {
      argParser.exitOnHelp(true);
      expect(argParser.exitHelp).to.equal(true);

      argParser.exitOnHelp(false);
      expect(argParser.exitHelp).to.equal(false);
    });
  });
});
