const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should disable usage in the help output', () => {
      argParser.disableHelpUsage();
      expect(argParser.usageHelpEnabled).to.equal(false);
    });
  });
});
