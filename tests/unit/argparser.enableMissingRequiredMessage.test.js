const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should enable the missing required message', () => {
      argParser.enableMissingRequiredMessage();
      expect(argParser.missingRequiredEnabled).to.equal(true);
    });
  });
});
