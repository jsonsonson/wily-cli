const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should build the CLI from a configuration', () => {
      const config = {
        commands: [
          ['command1', 'Command 1', () => { console.log('command 1'); }],
        ],
        options: [
          ['option1', 'Option 1', { required: true }],
        ],
        parameters: [
          ['parameter1', true, true],
        ],
        events: [
          ['exec', () => { console.log('executed'); }],
        ],
        args: process.argv,
        description: 'Description',
        defaultCommand: 'command1',
        parameterSeparator: '}',
        version: '0.0.1',
        versionOption: ['release', 'r'],
        versionOptionDescription: 'Get release number',
        helpOption: ['helpme', 'a'],
        helpOptionDescription: 'Get some help',
        usage: 'Usage',
        switchCharacter: '/',
        generateHelpEnabled: true,
        helpPadding: ']]]',
        beforeHelpLog: 'Before',
        afterHelpLog: 'After',
        exitOnHelp: false,
        exitOnVersion: false,
        helpUsageEnabled: true,
        helpCommandsEnabled: true,
        helpOptionsEnabled: true,
        helpDescriptionEnabled: true,
        optionsEnabled: true,
        commandsEnabled: true,
        interactive: {
          prompt: 'prompt$ ',
          quitCommand: 'stop',
          quitCommandDescription: 'Get out',
          quitCommandEnabled: true,
        },
        onResult: () => { console.log('onResult'); },
        missingRequiredMessageEnabled: true,
        missingRequiredMessage: 'You missed some things: ',
      };

      argParser.from(config);

      expect(argParser.commands.length).to.equal(1);
      expect(argParser.options.length).to.equal(2);
      expect(argParser.parameters.length).to.equal(1);
      expect(argParser.argv).to.equal(process.argv);
      expect(argParser.descriptionStr.includes('Description')).to.equal(true);
      expect(argParser.defCommand.includes('command1')).to.equal(true);
      expect(argParser.paramSeparator.includes('}')).to.equal(true);
      expect(argParser.vers.includes('0.0.1')).to.equal(true);
      expect(argParser.versionOpt.includes('release')).to.equal(true);
      expect(argParser.versionOptShort.includes('r')).to.equal(true);
      expect(argParser.helpOpt.includes('helpme')).to.equal(true);
      expect(argParser.helpOptShort.includes('a')).to.equal(true);
      expect(argParser.versionDescription.includes('Get release number')).to.equal(true);
      expect(argParser.options.find(option => option._n === argParser.helpOpt).description.includes('Get some help')).to.equal(true);
      expect(argParser.usageStr.includes('Usage')).to.equal(true);
      expect(argParser.cli.options._switchChar.includes('/')).to.equal(true);
      expect(argParser.generateHelp).to.equal(true);
      expect(argParser.helpPaddingStr.includes(']]]')).to.equal(true);
      expect(argParser.beforeHelpStr.includes('Before')).to.equal(true);
      expect(argParser.afterHelpStr.includes('After')).to.equal(true);
      expect(argParser.exitHelp).to.equal(false);
      expect(argParser.exitVersion).to.equal(false);
      expect(argParser.usageHelpEnabled).to.equal(true);
      expect(argParser.commandsHelpEnabled).to.equal(true);
      expect(argParser.optionsHelpEnabled).to.equal(true);
      expect(argParser.descriptionHelpEnabled).to.equal(true);
      expect(argParser.commandsEnabled).to.equal(true);
      expect(argParser.optionsEnabled).to.equal(true);
      expect(argParser.interactive).to.equal(true);
      expect(argParser.prompt.includes('prompt$ ')).to.equal(true);
      expect(argParser.quitCommand.includes('stop')).to.equal(true);
      expect(argParser.quitCommandDesc.includes('Get out')).to.equal(true);
      expect(argParser.quitEnabled).to.equal(true);
      expect(argParser.resultCallback).to.not.be.undefined;
      expect(argParser.missingRequiredEnabled).to.equal(true);
      expect(argParser.missingRequiredMessage.includes('You missed some things: ')).to.equal(true);
    });
  });
});
