const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    context('with an ArgParser action', () => {
      it('should add the command', () => {
        const secondCLI = cli();
        const cmdName = 'command3332';
        const tempCallerFile = argParser.callerFile;
        argParser.callerFile = __filename;
        argParser.command(cmdName, 'ArgParser', secondCLI);

        const cmd = argParser.commands.find(cmd => cmd._n === cmdName);
        cmd.build();
        argParser.callerFile = tempCallerFile;

        expect(cmd).to.not.be.undefined;
        expect(cmd.action).to.not.be.undefined;
      });
    });

    context('with a Function action', () => {
      it('should add the command', () => {
        const cmdName = 'command12234';
        const tempCallerFile = argParser.callerFile;
        argParser.callerFile = __filename;
        argParser.command(cmdName, 'Function', () => {});

        const cmd = argParser.commands.find(cmd => cmd._n === cmdName);
        cmd.build();
        argParser.callerFile = tempCallerFile;

        expect(cmd).to.not.be.undefined;
        expect(cmd.action).to.not.be.undefined;
      });
    });

    context('with an String action', () => {
      it('should add the command', () => {
        const cmdName = 'command12';
        const tempCallerFile = argParser.callerFile;
        argParser.callerFile = __filename;
        argParser.command(cmdName, 'String', './argparser.command.test-command.js');

        const cmd = argParser.commands.find(cmd => cmd._n === cmdName);
        cmd.build();
        argParser.callerFile = tempCallerFile;

        expect(cmd).to.not.be.undefined;
        expect(cmd.action).to.not.be.undefined;
      });
    });

    context('with an omitted action', () => {
      it('should add the command', () => {
        const cmdName = 'command';
        const tempCallerFile = argParser.callerFile;
        argParser.callerFile = __filename;
        argParser.command(cmdName, 'Omitted');

        const cmd = argParser.commands.find(cmd => cmd._n === cmdName);
        cmd.build();
        argParser.callerFile = tempCallerFile;

        expect(cmd).to.not.be.undefined;
        expect(cmd.action).to.not.be.undefined;
      });
    });
  });
});
