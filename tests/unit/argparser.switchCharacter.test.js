const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should set the switch character', () => {
      const char = '/';
      argParser.switchCharacter(char);

      expect(argParser.cli.options._switchChar.includes(char)).to.equal(true);
    });
  });
});
