const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const testArgParser = require('../../fixtures/testArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;

describe('wily-cli non-interactive', () => {
  describe('Options', () => {
    context('with an async function option event', () => {
      it('should execute the event', (done) => {
        testArgParser()
          .option('async', 'Async test')
          .on('async', async () => {
            expect(true).to.equal(true);
            done();
          })
          .parameter('paramyo')
          .parse(['node', __filename, '--async']);
      });
    });

    context('with a promise option event', () => {
      it('should execute the event', (done) => {
        const promiseEvent = () => {
          return new Promise(() => {
            expect(true).to.equal(true);
            done();
          });
        };

        testArgParser()
          .option('async', 'Async test')
          .on('async', promiseEvent())
          .parameter('paramyo')
          .parse(['node', __filename, '--async']);
      });
    });

    context('with an async function option event error', () => {
      it('should process the error in onError', (done) => {
        testArgParser()
          .option('async', 'Async test')
          .on('async', async () => {
            throw new Error('err');
          })
          .on('error', () => {
            expect(true).to.equal(true);
            done();
          })
          .parameter('paramyo')
          .parse(['node', __filename, '--async']);
      });
    });

    context('with a promise option event', () => {
      it('should process the error in onError', (done) => {
        const promiseEvent = () => {
          return new Promise(() => {
            throw new Error('err');
          });
        };

        testArgParser()
          .option('async', 'Async test')
          .on('async', promiseEvent())
          .on('error', () => {
            expect(true).to.equal(true);
            done();
          })
          .parameter('paramyo')
          .parse(['node', __filename, '--async']);
      });
    });

    context('with the help option', () => {
      it('should parse help', (done) => {
        const randomAP = testArgParser()
          .parameter('one')
          .parameterSeparator('+');

        testArgParser()
          .usage('[blue]Usage')
          .command('random', 'Random', randomAP)
          .option('defaultOpt <u> <v>', 'default option', {
            parameters: ['x', 'y'],
            default: { x: 1, y: 'two' },
          })
          .option('defaultOpt2', 'default option 2', {
            default: [1, 'two'],
          })
          .parameter('yooo')
          .on('help', (options) => {
            expect(options.help).to.not.be.undefined;
            done();
          })
          .parse(['node', __filename, '-h']);
      });
    });

    context('with the help option passed to a sub command', () => {
      it('should parse the sub commands\'s help', (done) => {
        const sub = testArgParser()
          .parameter('duuuude', true)
          .on('help', (options) => {
            expect(options.help).to.not.be.undefined;
            done();
          });

        testArgParser()
          .usage('Usage')
          .command('sub', 'Sub sub', sub)
          .parameter('yooo', true)
          .parse(['node', __filename, 'sub', '-h']);
      });
    });

    context('with no options provided and help enabled', () => {
      it('should parse help', (done) => {
        testArgParser()
          .parameter('yooo')
          .on('help', (options) => {
            expect(options.help).to.not.be.undefined;
            done();
          })
          .parse(['node', __filename]);
      });
    });

    context('with no options provided and help disabled', () => {
      it('should not parse help', (done) => {
        const result = testArgParser()
          .disableOptions()
          .parse(['node', __filename]);

        expect(result.options.help).to.be.undefined;
        done();
      });
    });

    context('with the version option', () => {
      it('should parse version', (done) => {
        testArgParser()
          .on('version', (options) => {
            expect(options.version).to.not.be.undefined;
            done();
          })
          .parse(['node', __filename, '-v']);
      });
    });

    context('with a on/off option', () => {
      it('should set the option and opposite option', (done) => {
        testArgParser()
          .option('no-cats', 'No cats')
          .on('no-cats', (options) => {
            expect(options['no-cats']).to.equal(true);
            expect(options['cats']).to.equal(false);
            done();
          })
          .parse(['node', __filename, '--no-cats']);
      });
    });

    context('with an option with no value', () => {
      it('should set the value to true if no default value', (done) => {
        testArgParser()
          .on('o1', (options) => {
            expect(options.o1).to.equal(true);
            done();
          })
          .option('defaultOpt', 'default option', {
            parameters: ['x', 'y'],
            default: { x: 1, y: 'two' },
            required: true,
          })
          .parse(['node', __filename, '--o1', '--defaultOpt']);
      });

      it('should set the value to default if a default value is set', (done) => {
        testArgParser()
          .on('o4', (options) => {
            expect(options.o4).to.equal('x');
            done();
          })
          .parse(['node', __filename, '--o4']);
      });
    });

    context('with a value passed to the option', () => {
      it('should set the value to true if no default value', (done) => {
        testArgParser()
          .on('o1', (options) => {
            expect(options.o1).to.equal('test');
            done();
          })
          .parse(['node', __filename, '--o1', 'test']);
      });
    });

    context('with an option that has a modifier', () => {
      it('should apply the modifier to the option value', (done) => {
        testArgParser()
          .on('o2', (options) => {
            expect(options.o2).to.equal(2);
            done();
          })
          .parse(['node', __filename, '--o2', '1']);
      });
    });

    context('with an option that has a set short', () => {
      it('should parse the option with the short value', (done) => {
        testArgParser()
          .on('o3', (options) => {
            expect(options.o3).to.not.be.undefined;
            done();
          })
          .parse(['node', __filename, '-o3', 'one', '2']);
      });
    });

    context('with a non default switch', () => {
      it('should parse the option', (done) => {
        testArgParser()
          .switchCharacter('/')
          .on('o1', (options) => {
            expect(options.o1).to.equal(1);
            done();
          })
          .parse(['node', __filename, '/o', '1']);
      });
    });

    context('with an option separated with :', () => {
      it('should return the correct value', (done) => {
        testArgParser()
          .on('o1', (options) => {
            expect(options.o1).to.equal('x');
            done();
          })
          .parse(['node', __filename, '-o:x']);
      });
    });

    context('with an option separated with =', () => {
      it('should return the correct value', (done) => {
        testArgParser()
          .on('o1', (options) => {
            expect(options.o1).to.equal('1');
            done();
          })
          .parse(['node', __filename, '-o=1']);
      });
    });

    context('with an option with multiple values', () => {
      it('should return an array of values', (done) => {
        testArgParser()
          .on('o1', (options) => {
            expect(options.o1).to.be.an('array');
            expect(options.o1[0]).to.equal(1);
            expect(options.o1[1]).to.equal(2);
            done();
          })
          .parse(['node', __filename, '-o', '1', '2', '--extraOption']);
      });
    });

    context('with missing required option', () => {
      it('should pass back the missing options', (done) => {
        const result = testArgParser()
          .enableMissingRequiredMessage()
          .option('missing', 'Missing option', { required: true })
          .parse(['node', __filename]);

        expect(result.missingOptions.length).to.not.equal(0);
        done();
      });
    });

    context('with a pass-through option', () => {
      it('should pass the option to sub commands', (done) => {
        const subCommand = testArgParser().command('subTest', 'Test', (options) => {
          expect(options.pass).to.equal(true);
          done();
        });

        testArgParser()
          .option('pass', 'Pass through', { passThrough: true })
          .command('sub', 'Sub test', subCommand)
          .parse(['node', __filename, '--pass', 'sub', 'subTest']);
      });
    });
  });

  describe('Commands', () => {
    context('with an async function command', () => {
      it('should execute the command', (done) => {
        testArgParser()
          .command('test', 'Test', async () => {
            expect(true).to.equal(true);
            done();
          })
          .parameter('paramyo')
          .parse(['node', __filename, '2', 'extra', 'test']);
      });
    });

    context('with a promise command', () => {
      it('should execute the command', (done) => {
        const promiseCommand = new Promise(() => {
          setTimeout(() => {
            expect(true).to.equal(true);
            done();
          }, 10);
        });

        testArgParser()
          .command('test', 'Test', promiseCommand)
          .parameter('paramyo')
          .parse(['node', __filename, '2', 'extra', 'test']);
      });
    });

    context('with an async onResult function', () => {
      it('should execute onResult', (done) => {
        testArgParser()
          .command('test', 'Test', () => {})
          .parameter('paramyo')
          .onResult(async () => {
            expect(true).to.equal(true);
            done();
          })
          .parse(['node', __filename, '2', 'extra', 'test']);
      });
    });

    context('with a promise onResult function', () => {
      it('should execute onResult', (done) => {
        const promiseOnResult = new Promise(() => {
          setTimeout(() => {
            expect(true).to.equal(true);
            done();
          }, 10);
        });

        testArgParser()
          .command('test', 'Test', () => {})
          .parameter('paramyo')
          .onResult(promiseOnResult)
          .parse(['node', __filename, '2', 'extra', 'test']);
      });
    });

    context('with an error in a function command', () => {
      it('should catch the error in with error event', (done) => {
        testArgParser()
          .command('test', 'Test', () => {
            throw new Error('err');
          })
          .parameter('paramyo')
          .on('error', () => {
            expect(true).to.equal(true);
            done();
          })
          .parse(['node', __filename, '2', 'extra', 'test']);
      });
    });

    context('with an error in a promise command', () => {
      it('should catch the error in with error event', (done) => {
        const promiseCommand = new Promise(() => {
          throw new Error('catch this');
        });

        testArgParser()
          .command('test', 'Test', promiseCommand)
          .parameter('paramyo')
          .on('error', () => {
            expect(true).to.equal(true);
            done();
          })
          .parse(['node', __filename, '2', 'extra', 'test']);
      });
    });

    context('with an error in an async command', () => {
      it('should catch the error in with error event', (done) => {
        testArgParser()
          .command('test', 'Test', async () => {
            throw new Error('catch this');
          })
          .parameter('paramyo')
          .on('error', () => {
            expect(true).to.equal(true);
            done();
          })
          .parse(['node', __filename, '2', 'extra', 'test']);
      });
    });

    context('with an error during parse', () => {
      it('should catch the error in with error event', (done) => {
        testArgParser()
          .command('test', 'Test', () => {})
          .on('thatOption', () => {
            throw new Error('catch this');
          })
          .option('thatOption', 'That option')
          .parameter('paramyo')
          .on('error', () => {
            expect(true).to.equal(true);
            done();
          })
          .parse(['node', __filename, '2', 'extra', 'test', '--thatOption']);
      });
    });

    context('with a function command', () => {
      it('should execute the command', (done) => {
        testArgParser()
          .command('test', 'Test', () => {
            expect(true).to.equal(true);
            done();
          })
          .parameter('paramyo')
          .parse(['node', __filename, '2', 'extra', 'test']);
      });
    });

    context('with an ArgParser command', () => {
      it('should execute the command', (done) => {
        const ap2 = testArgParser().on('exec', () => {
          expect(true).to.equal(true);
          done();
        });

        testArgParser()
          .command('test', 'Test', ap2)
          .parse(['node', __filename, 'test']);
      });
    });

    context('with a filename command', () => {
      it('should execute the command', (done) => {
        const ap = testArgParser();
        ap.callerFile = __filename;
        const result = ap
          .command('test', 'Test', '../../fixtures/filename.js')
          .parameter('paramyo', true, true)
          .parse(['node', __filename, '2', 'asdfa', 'test']);

        expect(result.command).to.equal('test');
        done();
      });
    });

    context('with an omitted command', () => {
      it('should execute the command', (done) => {
        const ap = testArgParser();
        const result = ap
          .command('c2', 'Test')
          .parse(['node', __filename, 'c2']);

        expect(result.command).to.equal('c2');
        done();
      });
    });

    context('with a parameters in the command argument', () => {
      it('should parse the parameters', (done) => {
        testArgParser()
          .command('test <x>', 'Test', (options, parameters) => {
            expect(parameters.x).to.equal('hi');
            done();
          })
          .parse(['node', __filename, 'test', 'hi']);
      });
    });

    context('with parameter separators in the command argument', () => {
      it('should parse the parameters', (done) => {
        testArgParser()
          .command('test <x> . <y>', 'Test', (options, parameters) => {
            expect(parameters.x).to.equal('hi');
            expect(parameters.y).to.equal('yo');
            done();
          })
          .parse(['node', __filename, 'test', 'hi', '.', 'yo']);
      });
    });

    context('with a sub sub command', () => {
      it('should execute the command', (done) => {
        const sub = testArgParser()
          .command('subsub', 'Sub sub', () => {
            expect(true).to.equal(true);
            done();
          });

        testArgParser()
          .command('sub', 'Sub', sub)
          .parse(['node', __filename, 'sub', 'subsub']);
      });
    });

    context('with missing required parameters', () => {
      it('should pass back the missing paramters', (done) => {
        const result = testArgParser()
          .enableMissingRequiredMessage()
          .parameter('missing', true, true)
          .parse(['node', __filename]);

        expect(result.missingParameters.length).to.not.equal(0);
        done();
      });
    });
  });

  describe('Binary', () => {
    context('with a binary sub command', () => {
      it('should execute the sub command', (done) => {
        let platform = process.platform;

        if (platform === 'linux') {
          platform = 'linux';
        } else if (platform === 'darwin') {
          platform = 'macos';
        } else {
          platform = 'win.exe';
        }

        const results = testArgParser()
          .command('test', 'Test', `../fixtures/binary-${platform}`)
          .parameter('paramyo')
          .parse(['node', __filename, '2', 'extra', 'test']);

        expect(results.command).to.equal('test');
        done();
      });
    });
  });
});
