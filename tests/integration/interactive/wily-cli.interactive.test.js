const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const testArgParser = require('../../fixtures/testArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;

describe('wily-cli interactive', () => {
  describe('Options', () => {
    context('with the help option', () => {
      it('should parse help', (done) => {
        testArgParser(true)
          .usage('[blue]Usage')
          .option('defaultOpt', 'default option', {
            parameters: ['x', 'y'],
            default: { x: 1, y: 'two' },
          })
          .option('defaultOpt2', 'default option 2', {
            default: [1, 'two'],
          })
          .parameter('yooo')
          .on('help', (options) => {
            expect(options.help).to.not.be.undefined;
            done();
          })
          .parse(['-h']);
      });
    });

    context('with the help option passed to a sub command', () => {
      it('should parse the sub commands\'s help', (done) => {
        const sub = testArgParser(true)
          .parameter('duuuude')
          .on('help', (options) => {
            expect(options.help).to.not.be.undefined;
            done();
          });

        testArgParser(true)
          .usage('Usage')
          .command('sub', 'Sub sub', sub)
          .parameter('yooo')
          .parse(['sub', '-h']);
      });
    });

    context('with no options provided and help enabled', () => {
      it('should parse help', (done) => {
        testArgParser(true)
          .on('help', (options) => {
            expect(options.help).to.not.be.undefined;
            done();
          })
          .parse([]);
      });
    });

    context('with no options provided and help disabled', () => {
      it('should not parse help', (done) => {
        const result = testArgParser(true)
          .disableOptions()
          .parse(['node', __filename]);

        expect(result.options.help).to.be.undefined;
        done();
      });
    });

    context('with the version option', () => {
      it('should parse version', (done) => {
        testArgParser(true)
          .on('version', (options) => {
            expect(options.version).to.not.be.undefined;
            done();
          })
          .parse(['-v']);
      });
    });

    context('with a on/off option', () => {
      it('should set the option and opposite option', (done) => {
        testArgParser(true)
          .option('no-cats', 'No cats')
          .on('no-cats', (options) => {
            expect(options['no-cats']).to.equal(true);
            expect(options['cats']).to.equal(false);
            done();
          })
          .parse(['--no-cats']);
      });
    });

    context('with an option with no value', () => {
      it('should set the value to true if no default value', (done) => {
        testArgParser(true)
          .on('o1', (options) => {
            expect(options.o1).to.equal(true);
            done();
          })
          .parse(['--o1']);
      });

      it('should set the value to default if a default value is set', (done) => {
        testArgParser(true)
          .on('o4', (options) => {
            expect(options.o4).to.equal('x');
            done();
          })
          .parse(['--o4']);
      });
    });

    context('with a value passed to the option', () => {
      it('should set the value to true if no default value', (done) => {
        testArgParser(true)
          .on('o1', (options) => {
            expect(options.o1).to.equal('test');
            done();
          })
          .parse(['--o1', 'test']);
      });
    });

    context('with an option that has a modifier', () => {
      it('should apply the modifier to the option value', (done) => {
        testArgParser(true)
          .on('o2', (options) => {
            expect(options.o2).to.equal(2);
            done();
          })
          .parse(['--o2', '1']);
      });
    });

    context('with an option that has a set short', () => {
      it('should parse the option with the short value', (done) => {
        testArgParser(true)
          .on('o3', (options) => {
            expect(options.o3).to.not.be.undefined;
            done();
          })
          .parse(['-o3', 'one', '2']);
      });
    });

    context('with a non default switch', () => {
      it('should parse the option', (done) => {
        testArgParser(true)
          .switchCharacter('/')
          .on('o1', (options) => {
            expect(options.o1).to.equal(1);
            done();
          })
          .parse(['/o', '1']);
      });
    });

    context('with an option separated with :', () => {
      it('should return the correct value', (done) => {
        testArgParser(true)
          .on('o1', (options) => {
            expect(options.o1).to.equal('x');
            done();
          })
          .parse(['-o:x']);
      });
    });

    context('with an option separated with =', () => {
      it('should return the correct value', (done) => {
        testArgParser(true)
          .on('o1', (options) => {
            expect(options.o1).to.equal('1');
            done();
          })
          .parse(['-o=1']);
      });
    });

    context('with an multiple values', () => {
      it('should return an array of values', (done) => {
        testArgParser(true)
          .on('o1', (options) => {
            expect(options.o1).to.be.an('array');
            expect(options.o1[0]).to.equal(1);
            expect(options.o1[1]).to.equal(2);
            done();
          })
          .parse(['-o', '1', '2', '--extraOption']);
      });
    });

    context('with missing required option', () => {
      it('should pass back the missing options', (done) => {
        const result = testArgParser(true)
          .enableMissingRequiredMessage()
          .option('missing', 'Missing option', { required: true })
          .parse([]);

        expect(result.missingOptions.length).to.not.equal(0);
        done();
      });
    });
  });

  describe('Commands', () => {
    context('with an async function command', () => {
      it('should execute the command', (done) => {
        testArgParser(true)
          .command('test', 'Test', async () => {
            expect(true).to.equal(true);
            done();
          })
          .parameter('paramyo')
          .parse(['2', 'extra', 'test']);
      });
    });

    context('with a promise command', () => {
      it('should execute the command', (done) => {
        const promiseCommand = new Promise(() => {
          setTimeout(() => {
            expect(true).to.equal(true);
            done();
          }, 10);
        });

        testArgParser(true)
          .command('test', 'Test', promiseCommand)
          .parameter('paramyo')
          .parse(['2', 'extra', 'test']);
      });
    });

    context('with an async onResult function', () => {
      it('should execute onResult', (done) => {
        testArgParser(true)
          .command('test', 'Test', () => {})
          .parameter('paramyo')
          .onResult(async () => {
            expect(true).to.equal(true);
            done();
          })
          .parse(['2', 'extra', 'test']);
      });
    });

    context('with a promise onResult function', () => {
      it('should execute onResult', (done) => {
        const promiseOnResult = new Promise(() => {
          setTimeout(() => {
            expect(true).to.equal(true);
            done();
          }, 10);
        });

        testArgParser(true)
          .command('test', 'Test', () => {})
          .parameter('paramyo')
          .onResult(promiseOnResult)
          .parse(['2', 'extra', 'test']);
      });
    });

    context('with an error in a function command', () => {
      it('should catch the error in with error event', (done) => {
        testArgParser(true)
          .command('test', 'Test', () => {
            throw new Error('err');
          })
          .parameter('paramyo')
          .on('error', () => {
            expect(true).to.equal(true);
            done();
          })
          .parse(['2', 'extra', 'test']);
      });
    });

    context('with an error in a promise command', () => {
      it('should catch the error in with error event', (done) => {
        const promiseCommand = new Promise(() => {
          throw new Error('catch this');
        });

        testArgParser(true)
          .command('test', 'Test', promiseCommand)
          .parameter('paramyo')
          .on('error', () => {
            expect(true).to.equal(true);
            done();
          })
          .parse(['2', 'extra', 'test']);
      });
    });

    context('with an error in an async command', () => {
      it('should catch the error in with error event', (done) => {
        testArgParser(true)
          .command('test', 'Test', async () => {
            throw new Error('catch this');
          })
          .parameter('paramyo')
          .on('error', () => {
            expect(true).to.equal(true);
            done();
          })
          .parse(['2', 'extra', 'test']);
      });
    });

    context('with an error during parse', () => {
      it('should catch the error in with error event', (done) => {
        testArgParser(true)
          .command('test', 'Test', () => {})
          .on('thatOption', () => {
            throw new Error('catch this');
          })
          .option('thatOption', 'That option')
          .parameter('paramyo')
          .on('error', () => {
            expect(true).to.equal(true);
            done();
          })
          .parse(['2', 'extra', 'test', '--thatOption']);
      });
    });

    context('with a function command', () => {
      it('should execute the command', (done) => {
        testArgParser(true)
          .command('test', 'Test', () => {
            expect(true).to.equal(true);
            done();
          })
          .parameter('paramyo')
          .parse(['2', 'extra', 'test']);
      });
    });

    context('with an ArgParser command', () => {
      it('should execute the command', (done) => {
        const ap2 = testArgParser(true).on('exec', () => {
          expect(true).to.equal(true);
          done();
        });

        testArgParser(true)
          .command('test', 'Test', ap2)
          .parse(['test']);
      });
    });

    context('with a filename command', () => {
      it('should execute the command', (done) => {
        const ap = testArgParser(true);
        ap.callerFile = __filename;
        const result = ap
          .command('test', 'Test', '../../fixtures/filename.js')
          .parameter('paramyo', true, true)
          .parse(['2', 'asdfa', 'test']);

        expect(result.command).to.equal('test');
        done();
      });
    });

    context('with an omitted command', () => {
      it('should execute the command', (done) => {
        const ap = testArgParser(true);
        const result = ap
          .command('c2', 'Test')
          .parse(['c2']);

        expect(result.command).to.equal('c2');
        done();
      });
    });

    context('with parameters in the command argument', () => {
      it('should parse the parameters', (done) => {
        testArgParser(true)
          .command('test <x>', 'Test', (options, parameters) => {
            expect(parameters.x).to.equal('hi');
            done();
          })
          .parse(['test', 'hi']);
      });
    });

    context('with parameter separators in the command argument', () => {
      it('should parse the parameters', (done) => {
        testArgParser(true)
          .command('test <x> . <y>', 'Test', (options, parameters) => {
            expect(parameters.x).to.equal('hi');
            expect(parameters.y).to.equal('yo');
            done();
          })
          .parse(['test', 'hi', '.', 'yo']);
      });
    });

    context('with a sub sub command', () => {
      it('should execute the command', (done) => {
        const sub = testArgParser()
          .command('subsub', 'Sub sub', () => {
            expect(true).to.equal(true);
            done();
          });

        testArgParser(true)
          .command('sub', 'Sub', sub)
          .parse(['sub', 'subsub']);
      });
    });

    context('with missing required parameters', () => {
      it('should pass back the missing paramters', (done) => {
        const result = testArgParser(true)
          .enableMissingRequiredMessage()
          .parameter('missing', true, true)
          .parse([]);

        expect(result.missingParameters.length).to.not.equal(0);
        done();
      });
    });
  });
});
