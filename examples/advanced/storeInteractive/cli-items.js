const cli = require('wily-cli');
const store = require(`${__dirname}/Store.js`);

const args = cli
  .command('add', 'Add items', (options, parameters, command) => {
    const amount = options.amount || 0;
    const name = options.name;

    if (name !== undefined) {
      store.addItem(name, amount);
    }
  })
  .command('remove', 'Remove items', (options, parameters, command) => {
    const amount = options.amount || 0;
    const name = options.name;

    if (name !== undefined) {
      store.removeItem(name, amount);
    }
  })
  .command('setPrice <item> <price>', 'Set an item\'s price', './setPrice')
  .command('view <item>', 'View an item\'s details') // looks for cli-items-view.js
  .command('list', 'List store inventory', store.listItems)
  .option('amount', 'Item amount to add or remove')
  .option('name', 'Item name to add or remove');

module.exports = args;
