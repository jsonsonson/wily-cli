const fs = require('fs');
const path = require('path');

const arg0 = process.argv[0];
const dir = path.dirname(path.basename(arg0) === 'node' ? process.argv[1] : arg0);
const inventory = `${dir}/inventory.json`;
let items = {};

try {
  items = JSON.parse(fs.readFileSync(inventory).toString());
} catch (err) {}

function writeInventory() {
  fs.writeFileSync(inventory, JSON.stringify(items, null, '  '));
}

function addItem(name, amount) {
  if (amount >= 0) {
    if (items[name] === undefined) {
      items[name] = {};
    }

    const currentAmount = items[name].amount || 0;
    const newAmount = currentAmount + amount;

    items[name].amount = newAmount;

    writeInventory();
  }
}

function itemAmount(item) {
  if (items[item] !== undefined && items[item].amount !== undefined) {
    return items[item].amount;
  }

  return 0;
}

function getItem(item) {
  if (items[item] !== undefined) {
    return items[item];
  }

  return {};
}

function setPrice(item, price) {
  if (items[item] !== undefined) {
    items[item].price = price;
    writeInventory();
  }
}

function getPrice(item) {
  if (items[item] !== undefined && items[item].price !== undefined) {
    return items[item].price;
  }

  return -1;
}

function removeItem(name, amount) {
  if (amount >= 0 && items[name] !== undefined) {
    const currentAmount = items[name] !== undefined ? items[name].amount : 0;
    let newAmount = currentAmount - amount;

    if (newAmount < 0) {
      newAmount = 0;
    }

    items[name].amount = newAmount;
    writeInventory();
  }
}

function listItems() {
  console.log(JSON.stringify(items, null, '  '));
}

function openStore() {
  items._opened = true;
  writeInventory();
}

function closeStore() {
  items._opened = false;
  writeInventory();
}

function isOpened() {
  return items._opened;
}

module.exports = {
  addItem,
  removeItem,
  setPrice,
  listItems,
  openStore,
  closeStore,
  isOpened,
  getPrice,
  itemAmount,
  getItem,
};
