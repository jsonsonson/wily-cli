const cli = require('wily-cli');
const store = require(`${__dirname}/Store.js`);

const args = cli
  .on('exec', (options, parameters, command) => {
    if (!store.isOpened()) {
      console.log('Store is closed. Come back later');
      return;
    }

    if (parameters.item !== undefined && parameters.amount !== undefined && parameters.cash !== undefined) {
      const total = store.getPrice(parameters.item) * parameters.amount;

      if (total >= 0 && parameters.cash >= total && parameters.amount <= store.itemAmount(parameters.item)) {
        store.removeItem(parameters.item, parameters.amount);
        console.log(`Bought ${parameters.amount} ${parameters.item}${parameters.amount != 1 ? 's' : ''} for $${total}`);
      }
    }
  });

module.exports = args;
