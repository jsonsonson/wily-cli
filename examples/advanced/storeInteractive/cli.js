const axios = require('axios');
const cli = require('wily-cli');
const store = require(`${__dirname}/Store.js`);
const purchaseCLI = require(`${__dirname}/purchaseCLI.js`);

cli
  .command('open', 'Open the store', store.openStore)
  .command('close', 'Close the store', store.closeStore)
  .command('isOpened', 'Check if the store is open', () => {
    console.log(store.isOpened() ? 'Opened' : 'Closed');
  })
  .command('items', 'Manage store inventory') // looks for cli-items file that exports an ArgParser
  .command('purchase <item>.<amount>.<cash>', 'Purchase an item', purchaseCLI)
  .command('chuck', 'Log a random Chuck Norris joke', async () => {
    const jokeResponse = await axios.get('http://api.icndb.com/jokes/random');
    console.log(jokeResponse.data.value.joke.replace(/&quot;/g, '"'));
  })
  .description('Store CLI example [yellow]:)[clear]')
  .defaultCommand('isOpened')
  .version('1.0.0')
  .enableInteractivity('store$', 'exit', 'Exit the store');
