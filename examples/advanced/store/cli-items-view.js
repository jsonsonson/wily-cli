const cli = require('wily-cli');
const store = require(`${__dirname}/Store.js`);

const args = cli
  .on('exec', (options, parameters, command) => {
    if (parameters.item !== undefined) {
      console.log(JSON.stringify(store.getItem(parameters.item), null, '  '));
    }
  });

module.exports = args;
