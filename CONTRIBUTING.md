# Contributing
When contributing to this repository, please first discuss the change you wish to make via creating an [issue](https://gitlab.com/jsonsonson/wily-cli/issues/new) with the owners of this repository before making a change.

## Merge Request Process
* Merge requests must be made to the [dev](https://gitlab.com/jsonsonson/wily-cli/tree/dev) branch. Merge requests made to the master branch will not be approved, and will be deleted.
* Ensure any additional dependencies and devDependencies within `package.json` are removed. The goal of this project is to not have any additional dependencies to avoid bloating dependent's installations.
* Ensure the only changes are to API files under the `lib` directory and `tests/unit` directory.
* If requested, add tests to the appropriate file. Test files are to follow the format `file.function.test.js`.
* Please, do not comment with any "ping" messages (messages to try to notify of the pull request or issue). All issues and pull requests will be reviewed and accepted or denied as seen fit. Issues and merge requests will be less likely to be reviewed, and may be removed, if there are "ping" messages.
* Ensure the pipeline passes. Merge requests will not be reviewed if the pipeline is failing.
